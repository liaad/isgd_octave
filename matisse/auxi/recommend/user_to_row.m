% Given a user_id, get the correspondent row in the user factor matrix 
function retval = user_to_row (usermap, user_id)
  [~, retval] = ismember(usermap,user_id);
  if numel(retval) == 0,
     retval = [0];
  end
end
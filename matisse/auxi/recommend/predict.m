% Predict scores for a user and a list of items 
function retval = predict(usermatrix, itemmatrix, user_row, item_rows)
  % List of dot products
  retval = usermatrix(user_row,:) * itemmatrix(item_rows,:)';
end

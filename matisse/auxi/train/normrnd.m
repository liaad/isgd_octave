function [out] = normrnd(mu, sigma, m, n)
	global seed;

	out = zeros(m, n);

	for i = 1:numel(out),
		% Box-Muller
	    [u, seed] = uniform_distributed_rand(seed);
	    [v, seed] = uniform_distributed_rand(seed);
	    
		R = -2 * log(u);
		theta = 2 * single(pi) * v;
	    y = R * cos(theta);

	    out(i) = y * sigma + mu;
	end
end

function [y, nextseed] = uniform_distributed_rand(seed)

	a = uint64(1664525);
	c = uint64(1013904223);
    
	nextseed = linear_congruential_rand(seed, a, c);
        
    y = single(nextseed) / single(uint32(4294967295));
end

function y = linear_congruential_rand(seed, a, c)
    % Use 64-bits to emulate overflow arithmetic
	seed64 = uint64(seed);
	
	y = uint32(mod(a * seed64 + c, uint64(4294967296)));
end

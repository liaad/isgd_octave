% Get user ids from list of rows in user factor matrix
function retval = row_to_user (usermap, rows)
  retval = usermap(rows);
end
% Get item ids from list of rows in item factor matrix
function retval = row_to_item (itemmap, rows)
  retval = itemmap(rows);
end

% Obtain a recommendation list with size n for a given user id
function [recs,scores] = recommend (usermap, itemmap, user_id, n)
  user_row = user_to_row(usermap, user_id);
  if user_row ~= 0,
    % Recommendations are sorted by proximity to value 1
    [val, ord] = sort(abs(1 - predict(user_row, 1:length(itemmap))));
    recs = row_to_item(itemmap, ord(1:n))';
    scores = val(1:n)';
  end
end

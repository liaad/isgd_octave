%!assume_indices_in_range
function p = randperm(n)
	p = zeros(1, n);

	%i : value to place
	for i = 1:n,
		%choose the pos'th (0 indexed) free position
		pos = floor(custom_rand() * (n - i + 1));

		index = 1;
		while 1,
			if p(index) == 0,
				% Position is free, see if we've skipped enough.
				if pos == 0,
					p(index) = i;
					break;
				end
				pos = pos - 1;
			end
			index = index + 1;
		end
	end
end
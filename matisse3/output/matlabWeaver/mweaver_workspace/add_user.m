% Add a user to the model (if non-existant)
function [retval] = add_user(user_id)
   global usermap usermatrix
   retval = user_to_row(user_id);
   if (retval < 0)
      usermap(end + 1) = user_id;
      usermatrix = [usermatrix; newrow()];
      retval = length(usermap);
   end
end

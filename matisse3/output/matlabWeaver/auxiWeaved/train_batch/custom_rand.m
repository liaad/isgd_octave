% Return value from 0 to 1
function [y] = custom_rand()
   global seed;
   [y, seed] = uniform_distributed_rand(seed);
end

function [y, nextseed] = uniform_distributed_rand(seed)
   a = uint64(1664525);
   c = uint64(1013904223);
   nextseed = linear_congruential_rand(seed, a, c);
   y = single(nextseed) / single(uint32(4294967295));
end

function [y] = linear_congruential_rand(seed, a, c)
   % Use 64-bits to emulate overflow arithmetic
   seed64 = uint64(seed);
   y = uint32(mod(a * seed64 + c, uint64(4294967296)));
end


% Given an item_id, get the correspondent row in the item factor matrix
function [retval] = item_to_row(item_id)
   global itemmap
   retval = find(item_id == itemmap);
   if isempty(retval)
      retval = -1;
   else
      retval = retval(1);
   end
end


% Add an item to the model (if non-existant)
function [retval] = add_item(item_id)
   global itemmap itemmatrix use_imputation itemqueue
   retval = item_to_row(item_id);
   if (retval < 0)
      itemmap(end + 1) = item_id;
      itemmatrix = [itemmatrix; newrow()];
      retval = length(itemmap);
   end
   if (use_imputation)
      itemqueue(find(itemqueue == retval)) = [];
      itemqueue = [itemqueue, retval];
   end
end


function [out] = normrnd(mu, sigma, m, n)
   out = zeros(m, n);
   for i = 1:numel(out)
      % Box-Muller
      u = custom_rand();
      v = custom_rand();
      R = -2 * log(u);
      theta = 2 * single(pi) * v;
      y = R * cos(theta);
      out(i) = y * sigma + mu;
   end
end


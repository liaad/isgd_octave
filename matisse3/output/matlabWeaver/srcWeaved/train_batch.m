% read csv file and perform batch training
function [usermatrix, itemmatrix, usermap_as_matrix, itemmap_as_matrix, itemqueue, idx] = train_batch(data_values, iter)
   global usermatrix itemmatrix usermap itemmap itemqueue
   user_ids = [];
   item_ids = [];
   for i = 1:size(data_values, 1)
      user_ids(end + 1) = data_values(i, 1);
      item_ids(end + 1) = data_values(i, 2);
   end
   for i = 1:iter
      idx = randperm(length(user_ids));
      for j = idx
         update_model(user_ids(j), item_ids(j), 1, false);
      end
   end
   usermap_as_matrix = str2double(usermap);
   itemmap_as_matrix = str2double(itemmap);
end


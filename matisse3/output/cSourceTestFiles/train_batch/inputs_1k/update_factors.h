/* Header file for update_factors */

#ifndef UPDATE_FACTORS_H
#define UPDATE_FACTORS_H

#include <inttypes.h>

/**
 * Update user and item factors
 */
void update_factors_iiibb_0(int user_row, int item_row, int truescore, int8_t update_users, int8_t update_items);

/**
 * Update user and item factors
 */
void update_factors_iiiui8ui8_0(int user_row, int item_row, int truescore, uint8_t update_users, uint8_t update_items);

#endif

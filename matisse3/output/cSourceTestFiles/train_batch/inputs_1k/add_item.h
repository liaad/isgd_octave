/* Header file for add_item */

#ifndef ADD_ITEM_H
#define ADD_ITEM_H

#include "lib/matlabstring.h"

/**
 * Add an item to the model (if non-existant)
 */
int add_item_S_1(matlab_string* item_id);

#endif

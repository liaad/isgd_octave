/* Header file for flip1d */

#ifndef FLIP1D_H
#define FLIP1D_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_i* flip1d_ti_row_1(tensor_i* x, tensor_i** restrict y);

#endif

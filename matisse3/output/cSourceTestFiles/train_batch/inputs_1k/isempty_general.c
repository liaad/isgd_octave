/* Implementation file for isempty_general */

#include "isempty_general.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>


/**
 */
int8_t isempty_general_ti_2d_1(tensor_i* X)
{

   
   return X->length == 0;
}

/* Implementation file for update_factors */

#include "globals.h"
#include <inttypes.h>
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "predict.h"
#include <stdlib.h>
#include "update_factors.h"


/**
 * Update user and item factors
 */
void update_factors_iiibb_0(int user_row, int item_row, int truescore, int8_t update_users, int8_t update_items)
{
   uint32_t end;
   double err;
   int i;
   tensor_d* itemmatrix_1 = NULL;
   tensor_d* itemmatrix_2 = NULL;
   tensor_d* itemmatrix_3 = NULL;
   tensor_d* itemmatrix_4 = NULL;
   tensor_d* usermatrix_1 = NULL;
   tensor_d* usermatrix_2 = NULL;
   tensor_d* usermatrix_3 = NULL;
   tensor_d* usermatrix_4 = NULL;

   err = (double) truescore - predict_ii_1(user_row, item_row);
   end = k;
   for(i = 1; (double) i <= (double) end; ++i){
      if(update_users){
         double delta_u;
         double plus_1;
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_4);
            copy_td_ptd(itemmatrix, &itemmatrix_4);
         }
         
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_3);
            copy_td_ptd(usermatrix, &usermatrix_3);
         }
         
         delta_u = err * itemmatrix_4->data[(item_row - 1) + (i - 1) * itemmatrix_4->shape[0]] - regularization * usermatrix_3->data[(user_row - 1) + (i - 1) * usermatrix_3->shape[0]];
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_1);
            copy_td_ptd(usermatrix, &usermatrix_1);
         }
         
         plus_1 = usermatrix_1->data[(user_row - 1) + (i - 1) * usermatrix_1->shape[0]] + learn_rate * delta_u;
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_4);
            copy_td_ptd(usermatrix, &usermatrix_4);
         }
         
         MATISSE_reserve_capacity_tdii_td(user_row, i, &usermatrix_4);
         usermatrix_4->data[(user_row - 1) + (i - 1) * usermatrix_4->shape[0]] = plus_1;
         // Inlined '$create_and_copy$td'
         if(usermatrix_4 != NULL){
            new_array_helper_d(usermatrix_4->shape, usermatrix_4->dims, &usermatrix);
            copy_td_ptd(usermatrix_4, &usermatrix);
         }
         
      }
      
      if(update_items){
         double delta_i;
         double plus;
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_2);
            copy_td_ptd(usermatrix, &usermatrix_2);
         }
         
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_3);
            copy_td_ptd(itemmatrix, &itemmatrix_3);
         }
         
         delta_i = err * usermatrix_2->data[(user_row - 1) + (i - 1) * usermatrix_2->shape[0]] - regularization * itemmatrix_3->data[(item_row - 1) + (i - 1) * itemmatrix_3->shape[0]];
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_2);
            copy_td_ptd(itemmatrix, &itemmatrix_2);
         }
         
         plus = itemmatrix_2->data[(item_row - 1) + (i - 1) * itemmatrix_2->shape[0]] + learn_rate * delta_i;
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_1);
            copy_td_ptd(itemmatrix, &itemmatrix_1);
         }
         
         MATISSE_reserve_capacity_tdii_td(item_row, i, &itemmatrix_1);
         itemmatrix_1->data[(item_row - 1) + (i - 1) * itemmatrix_1->shape[0]] = plus;
         // Inlined '$create_and_copy$td'
         if(itemmatrix_1 != NULL){
            new_array_helper_d(itemmatrix_1->shape, itemmatrix_1->dims, &itemmatrix);
            copy_td_ptd(itemmatrix_1, &itemmatrix);
         }
         
      }
      
   }
   
   tensor_free_d(&itemmatrix_1);
   tensor_free_d(&itemmatrix_2);
   tensor_free_d(&itemmatrix_3);
   tensor_free_d(&itemmatrix_4);
   tensor_free_d(&usermatrix_1);
   tensor_free_d(&usermatrix_2);
   tensor_free_d(&usermatrix_3);
   tensor_free_d(&usermatrix_4);
}

/**
 * Update user and item factors
 */
void update_factors_iiiui8ui8_0(int user_row, int item_row, int truescore, uint8_t update_users, uint8_t update_items)
{
   uint32_t end;
   double err;
   int i;
   tensor_d* itemmatrix_1 = NULL;
   tensor_d* itemmatrix_2 = NULL;
   tensor_d* itemmatrix_3 = NULL;
   tensor_d* itemmatrix_4 = NULL;
   tensor_d* usermatrix_1 = NULL;
   tensor_d* usermatrix_2 = NULL;
   tensor_d* usermatrix_3 = NULL;
   tensor_d* usermatrix_4 = NULL;

   err = (double) truescore - predict_ii_1(user_row, item_row);
   end = k;
   for(i = 1; (double) i <= (double) end; ++i){
      if(update_users){
         double delta_u;
         double plus_1;
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_4);
            copy_td_ptd(itemmatrix, &itemmatrix_4);
         }
         
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_3);
            copy_td_ptd(usermatrix, &usermatrix_3);
         }
         
         delta_u = err * itemmatrix_4->data[(item_row - 1) + (i - 1) * itemmatrix_4->shape[0]] - regularization * usermatrix_3->data[(user_row - 1) + (i - 1) * usermatrix_3->shape[0]];
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_1);
            copy_td_ptd(usermatrix, &usermatrix_1);
         }
         
         plus_1 = usermatrix_1->data[(user_row - 1) + (i - 1) * usermatrix_1->shape[0]] + learn_rate * delta_u;
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_4);
            copy_td_ptd(usermatrix, &usermatrix_4);
         }
         
         MATISSE_reserve_capacity_tdii_td(user_row, i, &usermatrix_4);
         usermatrix_4->data[(user_row - 1) + (i - 1) * usermatrix_4->shape[0]] = plus_1;
         // Inlined '$create_and_copy$td'
         if(usermatrix_4 != NULL){
            new_array_helper_d(usermatrix_4->shape, usermatrix_4->dims, &usermatrix);
            copy_td_ptd(usermatrix_4, &usermatrix);
         }
         
      }
      
      if(update_items){
         double delta_i;
         double plus;
         // Inlined '$create_and_copy$td'
         if(usermatrix != NULL){
            new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_2);
            copy_td_ptd(usermatrix, &usermatrix_2);
         }
         
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_3);
            copy_td_ptd(itemmatrix, &itemmatrix_3);
         }
         
         delta_i = err * usermatrix_2->data[(user_row - 1) + (i - 1) * usermatrix_2->shape[0]] - regularization * itemmatrix_3->data[(item_row - 1) + (i - 1) * itemmatrix_3->shape[0]];
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_2);
            copy_td_ptd(itemmatrix, &itemmatrix_2);
         }
         
         plus = itemmatrix_2->data[(item_row - 1) + (i - 1) * itemmatrix_2->shape[0]] + learn_rate * delta_i;
         // Inlined '$create_and_copy$td'
         if(itemmatrix != NULL){
            new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_1);
            copy_td_ptd(itemmatrix, &itemmatrix_1);
         }
         
         MATISSE_reserve_capacity_tdii_td(item_row, i, &itemmatrix_1);
         itemmatrix_1->data[(item_row - 1) + (i - 1) * itemmatrix_1->shape[0]] = plus;
         // Inlined '$create_and_copy$td'
         if(itemmatrix_1 != NULL){
            new_array_helper_d(itemmatrix_1->shape, itemmatrix_1->dims, &itemmatrix);
            copy_td_ptd(itemmatrix_1, &itemmatrix);
         }
         
      }
      
   }
   
   tensor_free_d(&itemmatrix_1);
   tensor_free_d(&itemmatrix_2);
   tensor_free_d(&itemmatrix_3);
   tensor_free_d(&itemmatrix_4);
   tensor_free_d(&usermatrix_1);
   tensor_free_d(&usermatrix_2);
   tensor_free_d(&usermatrix_3);
   tensor_free_d(&usermatrix_4);
}

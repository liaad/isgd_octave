/* Header file for find_dynamic1 */

#ifndef FIND_DYNAMIC1_H
#define FIND_DYNAMIC1_H

#include <inttypes.h>
#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_i* find_dynamic1_ti_row_1(tensor_i* X, tensor_i** restrict I_out);

/**
 */
tensor_i* find_dynamic1_tb_row_1(tensor_b* X, tensor_i** restrict I_out);

#endif

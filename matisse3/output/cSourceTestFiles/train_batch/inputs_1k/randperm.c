/* Implementation file for randperm */

#include "custom_rand.h"
#include "lib/matisse.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <math.h>
#include "randperm.h"
#include <stdlib.h>


/**
 */
tensor_d* randperm_i_1(int n, tensor_d** restrict p)
{
   int i;
   int iter;
   int p_numel;

   create_td_2(1, n, p);
   p_numel = (*p)->length;
   for(iter = 0; iter < p_numel; ++iter){
      (*p)->data[iter] = 0.0;
   }
   
   // i : value to place
   // choose the pos'th (0 indexed) free position
   for(i = 1; i <= n; ++i){
      int index_1;
      float mtimes_arg1;
      float pos;
      mtimes_arg1 = custom_rand_1();
      pos = floorf(mtimes_arg1 * (float) (n - i + 1));
      index_1 = 1;
      while(1){
         if((*p)->data[index_1 - 1] == 0){
            //  Position is free, see if we've skipped enough.
            if(pos == 0){
               (*p)->data[index_1 - 1] = (double) i;
               break;
            }
            
            pos -= 1.0f;
         }
         
         ++index_1;
      }
      
   }
   
   
   return *p;
}

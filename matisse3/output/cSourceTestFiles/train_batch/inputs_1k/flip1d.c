/* Implementation file for flip1d */

#include "flip1d.h"
#include "lib/matisse.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>


/**
 */
tensor_i* flip1d_ti_row_1(tensor_i* x, tensor_i** restrict y)
{
   double end;
   int i;
   int minus_arg1;
   int minus_arg1_1;

   // Inlined '$create_and_copy$ti'
   if(x != NULL){
      new_array_helper_i(x->shape, x->dims, y);
      copy_ti_pti(x, y);
   }
   
   end = (double) (*y)->length / 2.0;
   minus_arg1_1 = (*y)->length;
   minus_arg1 = (*y)->length;
   for(i = 1; i <= end; ++i){
      int value;
      value = (*y)->data[minus_arg1_1 - i + 1 - 1];
      (*y)->data[minus_arg1 - i - 1] = (*y)->data[i - 1];
      (*y)->data[i - 1] = value;
   }
   
   
   return *y;
}

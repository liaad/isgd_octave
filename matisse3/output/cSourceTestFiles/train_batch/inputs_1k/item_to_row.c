/* Implementation file for item_to_row */

#include "find_dynamic1.h"
#include "globals.h"
#include "isempty_general.h"
#include "item_to_row.h"
#include "lib/matlabstring.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>


/**
 * Given an item_id, get the correspondent row in the item factor matrix
 */
int item_to_row_S_1(matlab_string* item_id)
{
   tensor_S* eq_arg2 = NULL;
   tensor_i* find_arg1 = NULL;
   int iter;
   int numel_result;
   tensor_i* retval = NULL;
   int retval_1;

   // Inlined '$create_and_copy$tS'
   if(itemmap != NULL){
      new_array_helper_S(itemmap->shape, itemmap->dims, &eq_arg2);
      copy_tS_ptS(itemmap, &eq_arg2);
   }
   
   new_array_helper_i(eq_arg2->shape, eq_arg2->dims, &find_arg1);
   numel_result = eq_arg2->length;
   for(iter = 0; iter < numel_result; ++iter){
      find_arg1->data[iter] = matlab_string_equals(item_id, eq_arg2->data[iter]);
   }
   
   find_dynamic1_ti_row_1(find_arg1, &retval);
   if(isempty_general_ti_2d_1(retval)){
      retval_1 = -1;
   }else{
      retval_1 = retval->data[0];
   }
   
   tensor_free_S(&eq_arg2);
   tensor_free_i(&find_arg1);
   
   return retval_1;
}

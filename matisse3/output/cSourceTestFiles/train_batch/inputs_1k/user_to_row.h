/* Header file for user_to_row */

#ifndef USER_TO_ROW_H
#define USER_TO_ROW_H

#include "lib/matlabstring.h"

/**
 * Given a user_id, get the correspondent row in the user factor matrix
 */
int user_to_row_S_1(matlab_string* user_id);

#endif

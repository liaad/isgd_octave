/* Implementation file for delete_single_index_matrix */

#include "delete_single_index_matrix.h"
#include <inttypes.h>
#include "lib/general_matrix.h"
#include "lib/matisse.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "sort1.h"
#include <stdlib.h>


/**
 */
tensor_i* delete_single_index_matrix_titiii_row_row_1(tensor_i* in, tensor_i* indices, int unused_always0, int unused_always1, tensor_i** restrict out)
{
   int colon_arg2;
   int colon_arg2_1;
   int current_skip_index;
   int end;
   int i;
   tensor_i* in_arg1 = NULL;
   int in_arg1_dim1;
   int in_arg1_dim2;
   tensor_i* indices_1 = NULL;
   int indices_numel;
   int iter;
   int iter_1;
   int iter_2;
   int iter_3;
   int iter_4;
   int numel_result_2;
   int numel_result_3;
   tensor_i* out_1 = NULL;
   tensor_i* out_arg1 = NULL;
   int out_arg1_dim1;
   int out_arg1_dim2;
   int out_pos;
   tensor_i* size = NULL;
   tensor_i* size_1 = NULL;
   tensor_i* sort_arg1 = NULL;

   indices_numel = indices->length;
   create_ti_2(indices_numel, 1, &sort_arg1);
   for(iter = 0; iter < indices_numel; ++iter){
      sort_arg1->data[iter] = indices->data[iter];
   }
   
   sort1_ti_col_1(sort_arg1, &indices_1);
   //  Flatten, but preserve "row-ness"/"column-ness"
   //  Column matrices remain columns, other sizes are flattened into rows.
   colon_arg2_1 = in->length;
   create_ti_2(1, colon_arg2_1, &in_arg1);
   for(iter_4 = 1; iter_4 <= colon_arg2_1; ++iter_4){
      in_arg1->data[iter_4 - 1] = iter_4;
   }
   
   size_i(in_arg1, &size);
   size_multiargs_ti_2_of_2(in_arg1, &in_arg1_dim1, &in_arg1_dim2);
   if(in_arg1_dim1 == 1 || in_arg1_dim2 == 1){
      int in_size1;
      int in_size2;
      size_multiargs_ti_2_of_2(in, &in_size1, &in_size2);
      if(in_size1 == 1){
         int horzcat_result[2];
         // Inlined 'new_row_i_2'
         horzcat_result[0] = 1;
         horzcat_result[1] = in_arg1->length;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result, &size);
         copy_i1x2_pti(horzcat_result, &size);
      }else if(in_size2 == 1){
         int horzcat_result_1[2];
         // Inlined 'new_row_i_2'
         horzcat_result_1[0] = in_arg1->length;
         horzcat_result_1[1] = 1;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_1, &size);
         copy_i1x2_pti(horzcat_result_1, &size);
      }
      
   }
   
   numel_result_2 = in_arg1->length;
   new_array_ti_i(size, &out_1);
   for(iter_2 = 0; iter_2 < numel_result_2; ++iter_2){
      out_1->data[iter_2] = in->data[in_arg1->data[iter_2] - 1];
   }
   
   current_skip_index = 1;
   out_pos = 1;
   end = in->length;
   for(i = 1; i <= end; ++i){
      int8_t condition;
      while(1){
         int8_t not_arg1;
         not_arg1 = current_skip_index <= indices_1->length && indices_1->data[current_skip_index - 1] < i;
         if(!not_arg1){
            break;
         }
         
         ++current_skip_index;
      }
      
      condition = current_skip_index <= indices_1->length && indices_1->data[current_skip_index - 1] == i;
      if(condition){
         //  Skip
      }else{
         out_1->data[out_pos - 1] = in->data[i - 1];
         ++out_pos;
      }
      
   }
   
   colon_arg2 = out_pos - 1;
   create_ti_2(1, colon_arg2, &out_arg1);
   for(iter_3 = 1; iter_3 <= colon_arg2; ++iter_3){
      out_arg1->data[iter_3 - 1] = iter_3;
   }
   
   size_i(out_arg1, &size_1);
   size_multiargs_ti_2_of_2(out_arg1, &out_arg1_dim1, &out_arg1_dim2);
   if(out_arg1_dim1 == 1 || out_arg1_dim2 == 1){
      int out_size1;
      int out_size2;
      size_multiargs_ti_2_of_2(out_1, &out_size1, &out_size2);
      if(out_size1 == 1){
         int horzcat_result_2[2];
         // Inlined 'new_row_i_2'
         horzcat_result_2[0] = 1;
         horzcat_result_2[1] = out_arg1->length;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_2, &size_1);
         copy_i1x2_pti(horzcat_result_2, &size_1);
      }else if(out_size2 == 1){
         int horzcat_result_3[2];
         // Inlined 'new_row_i_2'
         horzcat_result_3[0] = out_arg1->length;
         horzcat_result_3[1] = 1;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_3, &size_1);
         copy_i1x2_pti(horzcat_result_3, &size_1);
      }
      
   }
   
   numel_result_3 = out_arg1->length;
   new_array_ti_i(size_1, out);
   for(iter_1 = 0; iter_1 < numel_result_3; ++iter_1){
      (*out)->data[iter_1] = out_1->data[out_arg1->data[iter_1] - 1];
   }
   
   tensor_free_i(&in_arg1);
   tensor_free_i(&indices_1);
   tensor_free_i(&out_1);
   tensor_free_i(&out_arg1);
   tensor_free_i(&size);
   tensor_free_i(&size_1);
   tensor_free_i(&sort_arg1);
   
   return *out;
}

/**
 */
tensor_i* delete_single_index_matrix_titiii_row_2d_1(tensor_i* in, tensor_i* indices, int unused_always0, int unused_always1, tensor_i** restrict out)
{
   int colon_arg2;
   int colon_arg2_1;
   int current_skip_index;
   int end;
   int i;
   tensor_i* in_arg1 = NULL;
   int in_arg1_dim1;
   int in_arg1_dim2;
   tensor_i* indices_1 = NULL;
   int indices_numel;
   int iter;
   int iter_1;
   int iter_2;
   int iter_3;
   int iter_4;
   int numel_result_2;
   int numel_result_3;
   tensor_i* out_1 = NULL;
   tensor_i* out_arg1 = NULL;
   int out_arg1_dim1;
   int out_arg1_dim2;
   int out_pos;
   tensor_i* size = NULL;
   tensor_i* size_1 = NULL;
   tensor_i* sort_arg1 = NULL;

   indices_numel = indices->length;
   create_ti_2(indices_numel, 1, &sort_arg1);
   for(iter = 0; iter < indices_numel; ++iter){
      sort_arg1->data[iter] = indices->data[iter];
   }
   
   sort1_ti_col_1(sort_arg1, &indices_1);
   //  Flatten, but preserve "row-ness"/"column-ness"
   //  Column matrices remain columns, other sizes are flattened into rows.
   colon_arg2_1 = in->length;
   create_ti_2(1, colon_arg2_1, &in_arg1);
   for(iter_4 = 1; iter_4 <= colon_arg2_1; ++iter_4){
      in_arg1->data[iter_4 - 1] = iter_4;
   }
   
   size_i(in_arg1, &size);
   size_multiargs_ti_2_of_2(in_arg1, &in_arg1_dim1, &in_arg1_dim2);
   if(in_arg1_dim1 == 1 || in_arg1_dim2 == 1){
      int in_size1;
      int in_size2;
      size_multiargs_ti_2_of_2(in, &in_size1, &in_size2);
      if(in_size1 == 1){
         int horzcat_result[2];
         // Inlined 'new_row_i_2'
         horzcat_result[0] = 1;
         horzcat_result[1] = in_arg1->length;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result, &size);
         copy_i1x2_pti(horzcat_result, &size);
      }else if(in_size2 == 1){
         int horzcat_result_1[2];
         // Inlined 'new_row_i_2'
         horzcat_result_1[0] = in_arg1->length;
         horzcat_result_1[1] = 1;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_1, &size);
         copy_i1x2_pti(horzcat_result_1, &size);
      }
      
   }
   
   numel_result_2 = in_arg1->length;
   new_array_ti_i(size, &out_1);
   for(iter_2 = 0; iter_2 < numel_result_2; ++iter_2){
      out_1->data[iter_2] = in->data[in_arg1->data[iter_2] - 1];
   }
   
   current_skip_index = 1;
   out_pos = 1;
   end = in->length;
   for(i = 1; i <= end; ++i){
      int8_t condition;
      while(1){
         int8_t not_arg1;
         not_arg1 = current_skip_index <= indices_1->length && indices_1->data[current_skip_index - 1] < i;
         if(!not_arg1){
            break;
         }
         
         ++current_skip_index;
      }
      
      condition = current_skip_index <= indices_1->length && indices_1->data[current_skip_index - 1] == i;
      if(condition){
         //  Skip
      }else{
         out_1->data[out_pos - 1] = in->data[i - 1];
         ++out_pos;
      }
      
   }
   
   colon_arg2 = out_pos - 1;
   create_ti_2(1, colon_arg2, &out_arg1);
   for(iter_3 = 1; iter_3 <= colon_arg2; ++iter_3){
      out_arg1->data[iter_3 - 1] = iter_3;
   }
   
   size_i(out_arg1, &size_1);
   size_multiargs_ti_2_of_2(out_arg1, &out_arg1_dim1, &out_arg1_dim2);
   if(out_arg1_dim1 == 1 || out_arg1_dim2 == 1){
      int out_size1;
      int out_size2;
      size_multiargs_ti_2_of_2(out_1, &out_size1, &out_size2);
      if(out_size1 == 1){
         int horzcat_result_2[2];
         // Inlined 'new_row_i_2'
         horzcat_result_2[0] = 1;
         horzcat_result_2[1] = out_arg1->length;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_2, &size_1);
         copy_i1x2_pti(horzcat_result_2, &size_1);
      }else if(out_size2 == 1){
         int horzcat_result_3[2];
         // Inlined 'new_row_i_2'
         horzcat_result_3[0] = out_arg1->length;
         horzcat_result_3[1] = 1;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_3, &size_1);
         copy_i1x2_pti(horzcat_result_3, &size_1);
      }
      
   }
   
   numel_result_3 = out_arg1->length;
   new_array_ti_i(size_1, out);
   for(iter_1 = 0; iter_1 < numel_result_3; ++iter_1){
      (*out)->data[iter_1] = out_1->data[out_arg1->data[iter_1] - 1];
   }
   
   tensor_free_i(&in_arg1);
   tensor_free_i(&indices_1);
   tensor_free_i(&out_1);
   tensor_free_i(&out_arg1);
   tensor_free_i(&size);
   tensor_free_i(&size_1);
   tensor_free_i(&sort_arg1);
   
   return *out;
}

/* Implementation file for sort1 */

#include "lib/matisse.h"
#include "lib/sort.h"
#include "lib/tensor_struct.h"
#include "sort1.h"
#include <stdlib.h>


/**
 */
tensor_i* sort1_ti_col_1(tensor_i* in, tensor_i** restrict out)
{

   sort_1ati(in, "ascend", out);
   
   return *out;
}

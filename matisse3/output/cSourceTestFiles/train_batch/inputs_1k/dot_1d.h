/* Header file for dot_1d */

#ifndef DOT_1D_H
#define DOT_1D_H

#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
double dot_1d_tdtd_row_col_1(tensor_d* A, tensor_d* B);

#endif

/* Header file for update_model */

#ifndef UPDATE_MODEL_H
#define UPDATE_MODEL_H

#include <inttypes.h>
#include "lib/matlabstring.h"

/**
 * Update the model with a user-item pair
 */
void update_model_SSib_0(matlab_string* user_id, matlab_string* item_id, int niter, int8_t imputation);

#endif

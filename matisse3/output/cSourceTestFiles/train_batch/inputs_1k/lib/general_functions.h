/* Header file for lib/general_functions */

#ifndef LIB_GENERAL_FUNCTIONS_H
#define LIB_GENERAL_FUNCTIONS_H

#include "matisse.h"
#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
tensor_d* transpose_alloc_d(tensor_d* input_matrix, tensor_d** restrict transposed_matrix);

#endif

/* Header file for lib/array_creators_alloc */

#ifndef LIB_ARRAY_CREATORS_ALLOC_H
#define LIB_ARRAY_CREATORS_ALLOC_H

#include "matisse.h"
#include "matlabstring.h"
#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
tensor_S* zeros_S2(int dim_1, int dim_2, tensor_S** restrict t);

/**
 */
tensor_d* zeros_d2(int dim_1, int dim_2, tensor_d** restrict t);

#endif

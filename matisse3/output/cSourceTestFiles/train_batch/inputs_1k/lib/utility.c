/* Implementation file for lib/utility */

#include <stdio.h>
#include <stdlib.h>
#include "tensor_struct.h"
#include "utility.h"


/**
 */
void write_matrix_2d_ti(tensor_i* matrix, const char * filename)
{
   
   int i, j;
   int zero = 0;
   
   // Write <VARIABLE_NAME>
   FILE *f = fopen(filename, "w");
   if (f == NULL)
   {
       printf("Error opening file '%s'!\n", filename);
       exit(1);
   }

   if (!matrix) {
       printf("Could not write matrix to '%s' because matrix is NULL\n", filename);
       exit(1);
   }

   if(matrix->dims > 2) {
		printf("Variable <VARIABLE_NAME> will not be saved, it has more than 2 dimensions.\n");
		fclose(f);
		return;
   }

   if (matrix->length == 0) {
	   // If the matrix is empty, we won't print it.
	   // We can't just let it reach the loop then we'll have a crash at GET (i, 0).
	   fclose(f);
	   return;
   }

   // Flattens any dimension above 2
   for(i = 0; i < matrix->shape[0]; i++) {

	   fprintf(f, "%4.17e", (double) matrix->data[i]);
	   for(j = 1; j < matrix->shape[1]; j++) {
		   fprintf(f, ",%4.17e", 0.0);
	   }
	   fprintf(f, "\n");
   }


   fclose(f);
}

/**
 */
void write_matrix_2d_td(tensor_d* matrix, const char * filename)
{
   
   int i, j;
   int zero = 0;
   
   // Write <VARIABLE_NAME>
   FILE *f = fopen(filename, "w");
   if (f == NULL)
   {
       printf("Error opening file '%s'!\n", filename);
       exit(1);
   }

   if (!matrix) {
       printf("Could not write matrix to '%s' because matrix is NULL\n", filename);
       exit(1);
   }

   if(matrix->dims > 2) {
		printf("Variable <VARIABLE_NAME> will not be saved, it has more than 2 dimensions.\n");
		fclose(f);
		return;
   }

   if (matrix->length == 0) {
	   // If the matrix is empty, we won't print it.
	   // We can't just let it reach the loop then we'll have a crash at GET (i, 0).
	   fclose(f);
	   return;
   }

   // Flattens any dimension above 2
   for(i = 0; i < matrix->shape[0]; i++) {

	   fprintf(f, "%4.17e", matrix->data[(i) + (zero) * matrix->shape[0]]);
	   for(j = 1; j < matrix->shape[1]; j++) {
		   fprintf(f, ",%4.17e", matrix->data[(i) + (j) * matrix->shape[0]]);
	   }
	   fprintf(f, "\n");
   }


   fclose(f);
}

/* Header file for lib/sort */

#ifndef LIB_SORT_H
#define LIB_SORT_H

#include "matisse.h"
#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
void sort_1ati(tensor_i* matrix, const char * order, tensor_i** restrict sorted_matrix);

/**
 */
int sort_1ati_comparer(const void* raw1, const void* raw2);

#endif

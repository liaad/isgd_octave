/* Header file for lib/utility */

#ifndef LIB_UTILITY_H
#define LIB_UTILITY_H

#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
void write_matrix_2d_ti(tensor_i* matrix, const char * filename);

/**
 */
void write_matrix_2d_td(tensor_d* matrix, const char * filename);

#endif

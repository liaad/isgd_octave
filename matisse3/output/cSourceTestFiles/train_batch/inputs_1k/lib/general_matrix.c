/* Implementation file for lib/general_matrix */

#include "general_matrix.h"
#include "matisse.h"
#include <stdlib.h>
#include "tensor_struct.h"


/**
 */
void size_multiargs_td_2_of_2(tensor_d* in, int* restrict out_1, int* restrict out_2)
{

   *out_1 = in->shape[0];
   *out_2 = in->shape[1];
}

/**
 */
void size_multiargs_ti_2_of_2(tensor_i* in, int* restrict out_1, int* restrict out_2)
{

   *out_1 = in->shape[0];
   *out_2 = in->shape[1];
}

/* Header file for lib/array_creators_dec */

#ifndef LIB_ARRAY_CREATORS_DEC_H
#define LIB_ARRAY_CREATORS_DEC_H

#include "matlabstring.h"

/**
 */
matlab_string** create_static_S1000x2(int in0, int in1, matlab_string* out[2000]);

#endif

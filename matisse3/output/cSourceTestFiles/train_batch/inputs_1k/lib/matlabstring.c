/* Implementation file for lib/matlabstring */

#include <assert.h>
#include "matlabstring.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**
 */
matlab_string* matlab_string_copy(matlab_string* in, matlab_string** restrict out)
{
if (*out != NULL) {
	free((*out)->data);
} else {
	*out = malloc(sizeof(matlab_string));
	if (*out == NULL) {
		fprintf(stderr, "Failed to allocate output string.\n");
		abort();
	}
}

assert(in != NULL);

(*out)->length = in->length;
(*out)->data = malloc(in->length + 1);
strcpy((*out)->data, in->data);

return *out;
}

/**
 */
matlab_string* matlab_string_make_empty(matlab_string** restrict out)
{
if (*out != NULL) {
	free((*out)->data);
} else {
	*out = malloc(sizeof(matlab_string));
}

(*out)->length = 0;
(*out)->data = malloc(1);
(*out)->data[0] = '\0';
}

/**
 */
int matlab_string_equals(matlab_string* str1, matlab_string* str2)
{
	if (str1->length != str2->length) {
		return 0;
	}

	return strcmp(str1->data, str2->data) == 0;
}

/**
 */
matlab_string* string_from_cstring(const char * str, matlab_string** restrict outStr)
{
	if (*outStr != NULL) {
		// FIXME: See if size is enough.
		free((*outStr)->data);
		free((*outStr));
	} else {
		*outStr = malloc(sizeof(matlab_string));
		if (*outStr == NULL) {
			fprintf(stderr, "Error: Failed to allocate space for string\n");
			abort();
		}
	}

	int length = strlen(str);
	(*outStr)->length = length;
	(*outStr)->data = malloc(length + 1);
	if ((*outStr)->data == NULL) {
		fprintf(stderr, "Error: Failed to allocate space for string data (length=%d)\n", length);
		free(*outStr);
		abort();
	}
	strcpy((*outStr)->data, str);

	return *outStr;
}

/**
 */
void matlab_string_free(matlab_string* in)
{
if (in != NULL) {
	free(in->data);
	free(in);
}}

/* Header file for lib/tensor */

#ifndef LIB_TENSOR_H
#define LIB_TENSOR_H

#include <inttypes.h>
#include "matisse.h"
#include "matlabstring.h"
#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
tensor_S* create_tS_2(int index_1, int index_2, tensor_S** restrict t);

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_S(tensor_S** restrict t);

/**
 */
tensor_d* create_td_2(int index_1, int index_2, tensor_d** restrict t);

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_d(tensor_d** restrict t);

/**
 */
tensor_S* MATISSE_reserve_capacity_tSi_tS(int index1, tensor_S** restrict out);

/**
 */
tensor_S* new_const_array_S2(int dim_1, int dim_2, matlab_string* value, tensor_S** restrict t);

/**
 */
void set_matrix_values_alloc_S(tensor_S* t, matlab_string* value);

/**
 */
tensor_S* new_array_S_2(int dim_1, int dim_2, tensor_S** restrict t);

/**
 */
tensor_S* new_array_helper_S(int* shape, int dims, tensor_S** restrict t);

/**
 */
int is_same_shape_alloc_S(tensor_S* t, int* restrict shape, int dims);

/**
 */
int length_alloc_S(tensor_S* t);

/**
 */
tensor_d* new_array_helper_d(int* shape, int dims, tensor_d** restrict t);

/**
 */
int is_same_shape_alloc_d(tensor_d* t, int* restrict shape, int dims);

/**
 */
tensor_i* new_array_helper_i(int* shape, int dims, tensor_i** restrict t);

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_i(tensor_i** restrict t);

/**
 */
int is_same_shape_alloc_i(tensor_i* t, int* restrict shape, int dims);

/**
 */
tensor_d* MATISSE_reserve_capacity_tdii_td(int index1, int index2, tensor_d** restrict out);

/**
 */
tensor_d* new_const_array_d2(int dim_1, int dim_2, double value, tensor_d** restrict t);

/**
 */
void set_matrix_values_alloc_d(tensor_d* t, double value);

/**
 */
tensor_d* new_array_d_2(int dim_1, int dim_2, tensor_d** restrict t);

/**
 */
void copy_alloc_d(tensor_d* input, tensor_d** restrict output);

/**
 */
int dim_size_alloc_d(tensor_d* t, int index);

/**
 */
tensor_i* size_i(tensor_i* t, tensor_i** restrict size_array);

/**
 */
int ndims_alloc_i(tensor_i* t);

/**
 */
tensor_i* new_array_i_2(int dim_1, int dim_2, tensor_i** restrict t);

/**
 */
tensor_i* new_array_ti_i(tensor_i* shape, tensor_i** restrict t);

/**
 */
tensor_i* create_ti_2(int index_1, int index_2, tensor_i** restrict t);

/**
 */
tensor_d* new_col_array_d_2(tensor_d* value1, tensor_d* value2, tensor_d** restrict t);

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_b(tensor_b** restrict t);

/**
 */
tensor_b* new_array_helper_b(int* shape, int dims, tensor_b** restrict t);

/**
 */
int is_same_shape_alloc_b(tensor_b* t, int* restrict shape, int dims);

/**
 */
void print_dim_alloc_d(tensor_d* t);

/**
 */
void print_dim_alloc_i(tensor_i* t);

#endif

/* Header file for lib/matlabstring */

#ifndef LIB_MATLABSTRING_H
#define LIB_MATLABSTRING_H

#include "matlabstring.h"

typedef struct {
	int length;
	char* data;
} matlab_string;


/**
 */
matlab_string* matlab_string_copy(matlab_string* in, matlab_string** restrict out);

/**
 */
matlab_string* matlab_string_make_empty(matlab_string** restrict out);

/**
 */
int matlab_string_equals(matlab_string* str1, matlab_string* str2);

/**
 */
matlab_string* string_from_cstring(const char * str, matlab_string** restrict outStr);

/**
 */
void matlab_string_free(matlab_string* in);

#endif

/* Implementation file for lib/sort */

#include "matisse.h"
#include "matrix.h"
#include "sort.h"
#include <stdlib.h>
#include "tensor.h"
#include "tensor_struct.h"


/**
 */
void sort_1ati(tensor_i* matrix, const char * order, tensor_i** restrict sorted_matrix)
{

   // We know that the order is 'ascend', so there is no need to check at runtime.
   new_array_helper_i(matrix->shape, matrix->dims, sorted_matrix);
   copy_ti_pti(matrix, sorted_matrix);
   qsort((*sorted_matrix)->data, (*sorted_matrix)->length, sizeof(int), sort_1ati_comparer);
}

/**
 */
int sort_1ati_comparer(const void* raw1, const void* raw2)
{
	int elem1 = *((const int*)raw1);
	int elem2 = *((const int*)raw2);

	if (elem1 < elem2) return -1;
	return elem1 > elem2;
}

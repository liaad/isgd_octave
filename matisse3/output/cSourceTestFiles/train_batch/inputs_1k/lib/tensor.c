/* Implementation file for lib/tensor */

#include "array_creators_alloc.h"
#include "general_matrix.h"
#include <inttypes.h>
#include "matisse.h"
#include "matlab_general.h"
#include "matlabstring.h"
#include <stdio.h>
#include <stdlib.h>
#include "tensor.h"
#include "tensor_struct.h"


/**
 */
tensor_S* create_tS_2(int index_1, int index_2, tensor_S** restrict t)
{
/*  create(int index_1, int index_2... tensor_d** t) */


	int TRIES = 2;
	int i;
	int length;
	int dims;
	
	/* If matrix is already allocated, free it */
	if(*t != NULL) {
	
		tensor_free_S(t);
		*t = NULL;
	}

	/* Create the tensor and return it. */	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_S*) malloc(sizeof(tensor_S));
		if (*t != NULL) {
			break;
		}
	}

	if (*t == NULL) {
		printf("ERROR: Could not allocate memory for the matrix structure\n");
		exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = index_1 * index_2;
	dims = 2;


	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (matlab_string**) malloc(sizeof(matlab_string*) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
		}
	}
	

	(*t)->length = length;

	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
	}
	
	(*t)->shape[0] = index_1;
	(*t)->shape[1] = index_2;

while (dims > 2 && (*t)->shape[dims - 1] == 1) {
	--dims;
}

for (i = 0; i < length; i++) {
((*t)->data[i]) = 0;

matlab_string_make_empty(&((*t)->data[i]));

}


	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_S(tensor_S** restrict t)
{

	/* If already null, return */
	if(*t == NULL) {
		return;
	}

	/* Free the values, if tensor owns the data */
	if((*t)->owns_data) {
		free((*t)->data);
		(*t)->data = NULL;
	}
	
	/* Free the shape data */
	free((*t)->shape);
	(*t)->shape = NULL;
	
	/* Free the tensor itself */
	free(*t);
	
	/* Set the pointer to null */
	*t = NULL;
}

/**
 */
tensor_d* create_td_2(int index_1, int index_2, tensor_d** restrict t)
{
/*  create(int index_1, int index_2... tensor_d** t) */


	int TRIES = 2;
	int i;
	int length;
	int dims;
	
	/* If matrix is already allocated, free it */
	if(*t != NULL) {
	
		tensor_free_d(t);
		*t = NULL;
	}

	/* Create the tensor and return it. */	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_d*) malloc(sizeof(tensor_d));
		if (*t != NULL) {
			break;
		}
	}

	if (*t == NULL) {
		printf("ERROR: Could not allocate memory for the matrix structure\n");
		exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = index_1 * index_2;
	dims = 2;


	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (double*) malloc(sizeof(double) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
		}
	}
	

	(*t)->length = length;

	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
	}
	
	(*t)->shape[0] = index_1;
	(*t)->shape[1] = index_2;

while (dims > 2 && (*t)->shape[dims - 1] == 1) {
	--dims;
}



	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_d(tensor_d** restrict t)
{

	/* If already null, return */
	if(*t == NULL) {
		return;
	}

	/* Free the values, if tensor owns the data */
	if((*t)->owns_data) {
		free((*t)->data);
		(*t)->data = NULL;
	}
	
	/* Free the shape data */
	free((*t)->shape);
	(*t)->shape = NULL;
	
	/* Free the tensor itself */
	free(*t);
	
	/* Set the pointer to null */
	*t = NULL;
}

/**
 */
tensor_S* MATISSE_reserve_capacity_tSi_tS(int index1, tensor_S** restrict out)
{
   int elements;
   int i1;
   tensor_S* in = NULL;
   int ncols;
   int nrows;

   in = *out;
   if(in == NULL){
      zeros_S2(1, index1, out);
      
      return *out;
   }
   
   elements = in->length;
   if(index1 <= elements){
      *out = in;
      
      return *out;
   }
   
   if(elements == 0){
      zeros_S2(1, index1, out);
      
      return *out;
   }
   
   if(in->dims > 2){
      printf("In assignment A(I) = B, a matrix A can not be resized");
      abort();
   }
   
   nrows = in->shape[0];
   ncols = in->shape[1];
   // In case *out == in:
   *out = NULL;
   if(nrows > 1){
      if(ncols > 1){
         printf("In assignment A(I) = B, a matrix A can not be resized");
         abort();
      }
      
      zeros_S2(index1, 1, out);
   }else{
      zeros_S2(1, index1, out);
   }
   
   // Copy old elements
   for(i1 = 0; i1 < elements; ++i1){
      matlab_string_copy(in->data[i1], &(*out)->data[i1]);
   }
   
   
   return *out;
}

/**
 */
tensor_S* new_const_array_S2(int dim_1, int dim_2, matlab_string* value, tensor_S** restrict t)
{

   new_array_S_2(dim_1, dim_2, t);
   if((*t)->owns_data) {
	   set_matrix_values_alloc_S(*t, value);
   } else {
      printf("WARNING (new_const_array): Call to zeros for an array that does not own its data.\n");
   }
   
   return *t;
}

/**
 */
void set_matrix_values_alloc_S(tensor_S* t, matlab_string* value)
{
/* set_matrix_values(tensor* t, elementType value) */

	int i;
	
	/* Set the values inside the tensor */
	for(i = 0; i<t->length; i = i+1)
   {
      t->data[i] = value;
   }
}

/**
 */
tensor_S* new_array_S_2(int dim_1, int dim_2, tensor_S** restrict t)
{
/* new_row(int dim1, int dim2..., tensor** t) */
	/* int* shape; */
	int shape[2];
	int dims;

	shape[0] = dim_1 > 0 ? dim_1: 0;
	shape[1] = dim_2 > 0 ? dim_2: 0;

/*	shape = (int[2]){<DIM_NAMES>}; */
	dims = 2;
	
	return new_array_helper_S(shape,  dims, t);	
}

/**
 */
tensor_S* new_array_helper_S(int* shape, int dims, tensor_S** restrict t)
{
/* new_row(int* shape, int dims, tensor** t) */

	int TRIES = 2;
	int i;
	int length;
	int sameShape;
	int* previous_shape = NULL;
	matlab_string** previous_data = NULL;
	
	/* Check if matrix is already allocated */
	if(*t != NULL) {
	
		/* If shape is the same, return current matrix, */
		/* even if it does not own the data (so it can implement window-writing) */
		sameShape = is_same_shape_alloc_S(*t, shape, dims);	
		//if(sameShape && (*t)->owns_data) {
		if(sameShape) {
			return *t;
		} 		
		
		/* Save pointers to previous shape and data */
		/* Only free data if tensor owns it */
		if((*t)->owns_data) {
			previous_data = (*t)->data;
		}
		
		previous_shape = (*t)->shape;		
	}

	/* Create the tensor and return it. */
	free(*t);
	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_S*) malloc(sizeof(tensor_S));
		if (*t != NULL) {
			break;
		}
	}
	/**t = (tensor_S*) malloc(sizeof(tensor_S)); */

	if (*t == NULL) {
       printf("ERROR: Could not allocate memory for the matrix structure\n");
	   exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = 1;
	for (i = 0; i < dims; i++) {
		length *= shape[i];
	}

	free(previous_data);
	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (matlab_string**) malloc(sizeof(matlab_string*) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
        }
	}
	

	(*t)->length = length;

	free(previous_shape);
	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
    }	
	
	for (i = 0; i < dims; ++i) {
		(*t)->shape[i] = shape[i];
	}

	while (dims > 2 && shape[dims - 1] == 1) {
		--dims;
	}

for (int i = 0; i < (*t)->length; ++i) {
(*t)->data[i] = 0;
;
matlab_string_make_empty(&(*t)->data[i]);
;
}

	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 */
int is_same_shape_alloc_S(tensor_S* t, int* restrict shape, int dims)
{
	int i;
	int result;
	
	/* Check if it has the same number of dimensions */
	if(t->dims != dims) {
		return 0;
	}

	/* As default, result is one */
	result = 1;
	
	/* Check if all dimensions of the tensor are the same */
	for(i=0; i<dims; i++) {
		if(t->shape[i] != shape[i]) {
			result = 0;
		}
	}

	/* Return result */
	
	return result;}

/**
 */
int length_alloc_S(tensor_S* t)
{
/* tensor* t */
	
	/* Determine the maximum dimension */
	int maxDim = 0;
	int i;
	int currentDim;
	
	for(i=0; i<t->dims; i++) {
		currentDim = t->shape[i];
		
		/* If empty matrix, return 0 */
		if(currentDim == 0) {
			return 0;
		}
		
		if(currentDim > maxDim) {
			maxDim = currentDim;
		}
	}
	
	return maxDim;
}

/**
 */
tensor_d* new_array_helper_d(int* shape, int dims, tensor_d** restrict t)
{
/* new_row(int* shape, int dims, tensor** t) */

	int TRIES = 2;
	int i;
	int length;
	int sameShape;
	int* previous_shape = NULL;
	double* previous_data = NULL;
	
	/* Check if matrix is already allocated */
	if(*t != NULL) {
	
		/* If shape is the same, return current matrix, */
		/* even if it does not own the data (so it can implement window-writing) */
		sameShape = is_same_shape_alloc_d(*t, shape, dims);	
		//if(sameShape && (*t)->owns_data) {
		if(sameShape) {
			return *t;
		} 		
		
		/* Save pointers to previous shape and data */
		/* Only free data if tensor owns it */
		if((*t)->owns_data) {
			previous_data = (*t)->data;
		}
		
		previous_shape = (*t)->shape;		
	}

	/* Create the tensor and return it. */
	free(*t);
	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_d*) malloc(sizeof(tensor_d));
		if (*t != NULL) {
			break;
		}
	}
	/**t = (tensor_d*) malloc(sizeof(tensor_d)); */

	if (*t == NULL) {
       printf("ERROR: Could not allocate memory for the matrix structure\n");
	   exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = 1;
	for (i = 0; i < dims; i++) {
		length *= shape[i];
	}

	free(previous_data);
	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (double*) malloc(sizeof(double) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
        }
	}
	

	(*t)->length = length;

	free(previous_shape);
	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
    }	
	
	for (i = 0; i < dims; ++i) {
		(*t)->shape[i] = shape[i];
	}

	while (dims > 2 && shape[dims - 1] == 1) {
		--dims;
	}



	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 */
int is_same_shape_alloc_d(tensor_d* t, int* restrict shape, int dims)
{
	int i;
	int result;
	
	/* Check if it has the same number of dimensions */
	if(t->dims != dims) {
		return 0;
	}

	/* As default, result is one */
	result = 1;
	
	/* Check if all dimensions of the tensor are the same */
	for(i=0; i<dims; i++) {
		if(t->shape[i] != shape[i]) {
			result = 0;
		}
	}

	/* Return result */
	
	return result;}

/**
 */
tensor_i* new_array_helper_i(int* shape, int dims, tensor_i** restrict t)
{
/* new_row(int* shape, int dims, tensor** t) */

	int TRIES = 2;
	int i;
	int length;
	int sameShape;
	int* previous_shape = NULL;
	int* previous_data = NULL;
	
	/* Check if matrix is already allocated */
	if(*t != NULL) {
	
		/* If shape is the same, return current matrix, */
		/* even if it does not own the data (so it can implement window-writing) */
		sameShape = is_same_shape_alloc_i(*t, shape, dims);	
		//if(sameShape && (*t)->owns_data) {
		if(sameShape) {
			return *t;
		} 		
		
		/* Save pointers to previous shape and data */
		/* Only free data if tensor owns it */
		if((*t)->owns_data) {
			previous_data = (*t)->data;
		}
		
		previous_shape = (*t)->shape;		
	}

	/* Create the tensor and return it. */
	free(*t);
	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_i*) malloc(sizeof(tensor_i));
		if (*t != NULL) {
			break;
		}
	}
	/**t = (tensor_i*) malloc(sizeof(tensor_i)); */

	if (*t == NULL) {
       printf("ERROR: Could not allocate memory for the matrix structure\n");
	   exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = 1;
	for (i = 0; i < dims; i++) {
		length *= shape[i];
	}

	free(previous_data);
	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (int*) malloc(sizeof(int) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
        }
	}
	

	(*t)->length = length;

	free(previous_shape);
	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
    }	
	
	for (i = 0; i < dims; ++i) {
		(*t)->shape[i] = shape[i];
	}

	while (dims > 2 && shape[dims - 1] == 1) {
		--dims;
	}



	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_i(tensor_i** restrict t)
{

	/* If already null, return */
	if(*t == NULL) {
		return;
	}

	/* Free the values, if tensor owns the data */
	if((*t)->owns_data) {
		free((*t)->data);
		(*t)->data = NULL;
	}
	
	/* Free the shape data */
	free((*t)->shape);
	(*t)->shape = NULL;
	
	/* Free the tensor itself */
	free(*t);
	
	/* Set the pointer to null */
	*t = NULL;
}

/**
 */
int is_same_shape_alloc_i(tensor_i* t, int* restrict shape, int dims)
{
	int i;
	int result;
	
	/* Check if it has the same number of dimensions */
	if(t->dims != dims) {
		return 0;
	}

	/* As default, result is one */
	result = 1;
	
	/* Check if all dimensions of the tensor are the same */
	for(i=0; i<dims; i++) {
		if(t->shape[i] != shape[i]) {
			result = 0;
		}
	}

	/* Return result */
	
	return result;}

/**
 */
tensor_d* MATISSE_reserve_capacity_tdii_td(int index1, int index2, tensor_d** restrict out)
{
   int elements1;
   int elements2;
   int i1;
   tensor_d* in = NULL;

   in = *out;
   if(in == NULL){
      zeros_d2(index1, index2, out);
      
      return *out;
   }
   
   size_multiargs_td_2_of_2(in, &elements1, &elements2);
   if(index1 <= elements1 && index2 <= elements2){
      *out = in;
      
      return *out;
   }
   
   if(elements1 == 0 || elements2 == 0){
      zeros_d2(index1, index2, out);
      
      return *out;
   }
   
   if(in->dims > 2){
      printf("In assignment A(I) = B, a matrix A can not be resized");
      abort();
   }
   
   // In case *out == in:
   *out = NULL;
   zeros_d2(max_scalars_dec_ii(index1, elements1), max_scalars_dec_ii(index2, elements2), out);
   // Copy old elements
   for(i1 = 0; i1 < elements1; ++i1){
      int i2;
      for(i2 = 0; i2 < elements2; ++i2){
         (*out)->data[(i1) + (i2) * (*out)->shape[0]] = in->data[(i1) + (i2) * in->shape[0]];
      }
      
   }
   
   
   return *out;
}

/**
 */
tensor_d* new_const_array_d2(int dim_1, int dim_2, double value, tensor_d** restrict t)
{

   new_array_d_2(dim_1, dim_2, t);
   if((*t)->owns_data) {
	   set_matrix_values_alloc_d(*t, value);
   } else {
      printf("WARNING (new_const_array): Call to zeros for an array that does not own its data.\n");
   }
   
   return *t;
}

/**
 */
void set_matrix_values_alloc_d(tensor_d* t, double value)
{
/* set_matrix_values(tensor* t, elementType value) */

	int i;
	
	/* Set the values inside the tensor */
	for(i = 0; i<t->length; i = i+1)
   {
      t->data[i] = value;
   }
}

/**
 */
tensor_d* new_array_d_2(int dim_1, int dim_2, tensor_d** restrict t)
{
/* new_row(int dim1, int dim2..., tensor** t) */
	/* int* shape; */
	int shape[2];
	int dims;

	shape[0] = dim_1 > 0 ? dim_1: 0;
	shape[1] = dim_2 > 0 ? dim_2: 0;

/*	shape = (int[2]){<DIM_NAMES>}; */
	dims = 2;
	
	return new_array_helper_d(shape,  dims, t);	
}

/**
 */
void copy_alloc_d(tensor_d* input, tensor_d** restrict output)
{
/*void copy_d(t* input, t** output) */

   int i;
   
   /* Prepare the output matrix */
   new_array_helper_d(input->shape, input->dims, output);
	
	/* Verify if they are the same array */
	if(input == *output) {
		return;
	}
	
	/*for(i = 1; i<=input->length; i = i+1) */
	for(i = 0; i<input->length; i = i+1)
	{
		(*output)->data[i] = input->data[i];
	}
}

/**
 */
int dim_size_alloc_d(tensor_d* t, int index)
{
	
	/* Check if given index is not bigger than the tensor */
	if(index >= t->dims) {
		return 1;
	}

	return t->shape[index];
}

/**
 */
tensor_i* size_i(tensor_i* t, tensor_i** restrict size_array)
{
   int i;

   new_array_i_2(1, ndims_alloc_i(t), size_array);
   for(i = 0; i < (ndims_alloc_i(t)); ++i){
      (*size_array)->data[i] = t->shape[i];
   }
   
   
   return *size_array;
}

/**
 */
int ndims_alloc_i(tensor_i* t)
{
   return t->dims;
}

/**
 */
tensor_i* new_array_i_2(int dim_1, int dim_2, tensor_i** restrict t)
{
/* new_row(int dim1, int dim2..., tensor** t) */
	/* int* shape; */
	int shape[2];
	int dims;

	shape[0] = dim_1 > 0 ? dim_1: 0;
	shape[1] = dim_2 > 0 ? dim_2: 0;

/*	shape = (int[2]){<DIM_NAMES>}; */
	dims = 2;
	
	return new_array_helper_i(shape,  dims, t);	
}

/**
 */
tensor_i* new_array_ti_i(tensor_i* shape, tensor_i** restrict t)
{
/*tensor_d** new_array_d(tensor_i* shape, tensor_d** t) */
	int dims = shape->length;
	int* newShape = malloc(sizeof(int) * dims);
	int i;

	for(i=0;i<dims;i++) {
		newShape[i] = shape->data[i];

	}

	new_array_helper_i(newShape,  dims, t);
	free(newShape);
	return *t;
}

/**
 */
tensor_i* create_ti_2(int index_1, int index_2, tensor_i** restrict t)
{
/*  create(int index_1, int index_2... tensor_d** t) */


	int TRIES = 2;
	int i;
	int length;
	int dims;
	
	/* If matrix is already allocated, free it */
	if(*t != NULL) {
	
		tensor_free_i(t);
		*t = NULL;
	}

	/* Create the tensor and return it. */	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_i*) malloc(sizeof(tensor_i));
		if (*t != NULL) {
			break;
		}
	}

	if (*t == NULL) {
		printf("ERROR: Could not allocate memory for the matrix structure\n");
		exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = index_1 * index_2;
	dims = 2;


	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (int*) malloc(sizeof(int) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
		}
	}
	

	(*t)->length = length;

	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
	}
	
	(*t)->shape[0] = index_1;
	(*t)->shape[1] = index_2;

while (dims > 2 && (*t)->shape[dims - 1] == 1) {
	--dims;
}



	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 */
tensor_d* new_col_array_d_2(tensor_d* value1, tensor_d* value2, tensor_d** restrict t)
{
   int col;
   int error;
   int numCols;
   int numRows;
   int outPos;

   error = 0;
   numRows = 0;
   numCols = 0;
   if(value1->length != 0){
      if(value1->shape[1] != numCols){
         if(numCols == 0){
            numRows += value1->shape[0];
            numCols = value1->shape[1];
         }else{
            error = 1;
         }
         
      }else{
         numRows += value1->shape[0];
      }
      
   }
   
   if(value2->length != 0){
      if(value2->shape[1] != numCols){
         if(numCols == 0){
            numRows += value2->shape[0];
            numCols = value2->shape[1];
         }else{
            error = 1;
         }
         
      }else{
         numRows += value2->shape[0];
      }
      
   }
   
   
   if(error){
      printf("Dimensions of matrices being concatenated are not consistent.\n");
      exit(EXIT_FAILURE);
   }
   
   
   new_array_d_2(numRows, numCols, t);
   outPos = 0;
   for(col = 0; col < numCols; ++col){
      int row;
      for(row = 0; row < value1->shape[0]; ++row){
         (*t)->data[outPos] = value1->data[row + col * value1->shape[0]];
         ++outPos;
      }
      
      for(row = 0; row < value2->shape[0]; ++row){
         (*t)->data[outPos] = value2->data[row + col * value2->shape[0]];
         ++outPos;
      }
      
   }
   
   
   return *t;
}

/**
 *  Deallocates the space used by the tensor;
 * 
 *  @param t
 *  		the tensor to free
 */
void tensor_free_b(tensor_b** restrict t)
{

	/* If already null, return */
	if(*t == NULL) {
		return;
	}

	/* Free the values, if tensor owns the data */
	if((*t)->owns_data) {
		free((*t)->data);
		(*t)->data = NULL;
	}
	
	/* Free the shape data */
	free((*t)->shape);
	(*t)->shape = NULL;
	
	/* Free the tensor itself */
	free(*t);
	
	/* Set the pointer to null */
	*t = NULL;
}

/**
 */
tensor_b* new_array_helper_b(int* shape, int dims, tensor_b** restrict t)
{
/* new_row(int* shape, int dims, tensor** t) */

	int TRIES = 2;
	int i;
	int length;
	int sameShape;
	int* previous_shape = NULL;
	int8_t* previous_data = NULL;
	
	/* Check if matrix is already allocated */
	if(*t != NULL) {
	
		/* If shape is the same, return current matrix, */
		/* even if it does not own the data (so it can implement window-writing) */
		sameShape = is_same_shape_alloc_b(*t, shape, dims);	
		//if(sameShape && (*t)->owns_data) {
		if(sameShape) {
			return *t;
		} 		
		
		/* Save pointers to previous shape and data */
		/* Only free data if tensor owns it */
		if((*t)->owns_data) {
			previous_data = (*t)->data;
		}
		
		previous_shape = (*t)->shape;		
	}

	/* Create the tensor and return it. */
	free(*t);
	
	for(i=0; i<TRIES; i++) {
		*t = (tensor_b*) malloc(sizeof(tensor_b));
		if (*t != NULL) {
			break;
		}
	}
	/**t = (tensor_b*) malloc(sizeof(tensor_b)); */

	if (*t == NULL) {
       printf("ERROR: Could not allocate memory for the matrix structure\n");
	   exit(EXIT_FAILURE);
	}

	/* Calculate the length of the linearized version of the tensor. */
	length = 1;
	for (i = 0; i < dims; i++) {
		length *= shape[i];
	}

	free(previous_data);
	if(length == 0) {
		(*t)->data = NULL;
	} else {
		(*t)->data = (int8_t*) malloc(sizeof(int8_t) * length);
		if((*t)->data == NULL) {
			printf("ERROR: Could not allocate memory for the matrix elements (%d elements)\n", length);
			exit(EXIT_FAILURE);
        }
	}
	

	(*t)->length = length;

	free(previous_shape);
	(*t)->shape = (int*) malloc(sizeof(int) * dims);
	if((*t)->shape == NULL) {
		printf("ERROR: Could not allocated memory for the matrix shape\n");
		exit(EXIT_FAILURE);
    }	
	
	for (i = 0; i < dims; ++i) {
		(*t)->shape[i] = shape[i];
	}

	while (dims > 2 && shape[dims - 1] == 1) {
		--dims;
	}



	(*t)->dims = dims;

	/* Data is owned by the tensor, since it allocated it */
	(*t)->owns_data = 1;
	
	return *t;
}

/**
 */
int is_same_shape_alloc_b(tensor_b* t, int* restrict shape, int dims)
{
	int i;
	int result;
	
	/* Check if it has the same number of dimensions */
	if(t->dims != dims) {
		return 0;
	}

	/* As default, result is one */
	result = 1;
	
	/* Check if all dimensions of the tensor are the same */
	for(i=0; i<dims; i++) {
		if(t->shape[i] != shape[i]) {
			result = 0;
		}
	}

	/* Return result */
	
	return result;}

/**
 */
void print_dim_alloc_d(tensor_d* t)
{

	int i;

	if(t->dims == 0) {
		return;
	}
	
	printf("[%d", t->shape[0]);
	for(i = 1; i< t->dims; i++) {
		printf(", %d", t->shape[i]);
	}
	printf("]");
}

/**
 */
void print_dim_alloc_i(tensor_i* t)
{

	int i;

	if(t->dims == 0) {
		return;
	}
	
	printf("[%d", t->shape[0]);
	for(i = 1; i< t->dims; i++) {
		printf(", %d", t->shape[i]);
	}
	printf("]");
}

/* Header file for lib/matrix */

#ifndef LIB_MATRIX_H
#define LIB_MATRIX_H

#include "matisse.h"
#include "matlabstring.h"
#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
tensor_d* copy_td_ptd(tensor_d* source_matrix, tensor_d** restrict target_matrix);

/**
 */
tensor_S* make_empty_tS(tensor_S** restrict out);

/**
 */
tensor_S* copy_tS_ptS(tensor_S* source_matrix, tensor_S** restrict target_matrix);

/**
 */
tensor_i* copy_ti_pti(tensor_i* source_matrix, tensor_i** restrict target_matrix);

/**
 */
tensor_i* create_from_matrix_i1x2_i(int in[2], tensor_i** restrict out);

/**
 */
tensor_i* copy_i1x2_pti(int source_matrix[2], tensor_i** restrict target_matrix);

/**
 */
tensor_i* make_empty_ti(tensor_i** restrict out);

/**
 */
tensor_d* make_empty_td(tensor_d** restrict out);

#endif

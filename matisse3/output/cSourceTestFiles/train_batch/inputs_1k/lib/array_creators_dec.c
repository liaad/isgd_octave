/* Implementation file for lib/array_creators_dec */

#include "array_creators_dec.h"
#include "matlabstring.h"


/**
 */
matlab_string** create_static_S1000x2(int in0, int in1, matlab_string* out[2000])
{
for (int i = 0; i < 2000; i++) {
	(out[i]) = 0;
	matlab_string_make_empty(&(out[i]));
}

return out;
}

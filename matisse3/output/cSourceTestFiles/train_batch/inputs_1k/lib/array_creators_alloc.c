/* Implementation file for lib/array_creators_alloc */

#include "array_creators_alloc.h"
#include "matisse.h"
#include "matlabstring.h"
#include <stdlib.h>
#include "tensor.h"
#include "tensor_struct.h"


/**
 */
tensor_S* zeros_S2(int dim_1, int dim_2, tensor_S** restrict t)
{

   
   return new_const_array_S2(dim_1, dim_2, NULL, t);
}

/**
 */
tensor_d* zeros_d2(int dim_1, int dim_2, tensor_d** restrict t)
{

   
   return new_const_array_d2(dim_1, dim_2, 0.0, t);
}

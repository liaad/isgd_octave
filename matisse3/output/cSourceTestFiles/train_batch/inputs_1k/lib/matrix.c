/* Implementation file for lib/matrix */

#include "matisse.h"
#include "matlabstring.h"
#include "matrix.h"
#include <stdlib.h>
#include "tensor.h"
#include "tensor_struct.h"


/**
 */
tensor_d* copy_td_ptd(tensor_d* source_matrix, tensor_d** restrict target_matrix)
{
   int i;

   for(i = 0; i < source_matrix->length; ++i){
      (*target_matrix)->data[i] = source_matrix->data[i];
   }
   
   
   return *target_matrix;
}

/**
 */
tensor_S* make_empty_tS(tensor_S** restrict out)
{

   create_tS_2(0, 0, out);
   
   return *out;
}

/**
 */
tensor_S* copy_tS_ptS(tensor_S* source_matrix, tensor_S** restrict target_matrix)
{
   int i;

   for(i = 0; i < source_matrix->length; ++i){
      matlab_string_copy(source_matrix->data[i], &(*target_matrix)->data[i]);
   }
   
   
   return *target_matrix;
}

/**
 */
tensor_i* copy_ti_pti(tensor_i* source_matrix, tensor_i** restrict target_matrix)
{
   int i;

   for(i = 0; i < source_matrix->length; ++i){
      (*target_matrix)->data[i] = source_matrix->data[i];
   }
   
   
   return *target_matrix;
}

/**
 */
tensor_i* create_from_matrix_i1x2_i(int in[2], tensor_i** restrict out)
{
   int shape[2];

   shape[0] = 1;
   shape[1] = 2;
   new_array_helper_i(shape, 2, out);
   
   return *out;
}

/**
 */
tensor_i* copy_i1x2_pti(int source_matrix[2], tensor_i** restrict target_matrix)
{
   int i;

   for(i = 0; i < (2); ++i){
      (*target_matrix)->data[i] = source_matrix[i];
   }
   
   
   return *target_matrix;
}

/**
 */
tensor_i* make_empty_ti(tensor_i** restrict out)
{

   create_ti_2(0, 0, out);
   
   return *out;
}

/**
 */
tensor_d* make_empty_td(tensor_d** restrict out)
{

   create_td_2(0, 0, out);
   
   return *out;
}

/* Implementation file for lib/general_functions */

#include "general_functions.h"
#include "matisse.h"
#include <stdio.h>
#include <stdlib.h>
#include "tensor.h"
#include "tensor_struct.h"


/**
 */
tensor_d* transpose_alloc_d(tensor_d* input_matrix, tensor_d** restrict transposed_matrix)
{
/* tensor* input_matrix, tensor** transposed_matrix */

	tensor_d* temp = NULL;
	int i;
	int j;
	int dim1, dim2;

	if (input_matrix->dims != 2) {
		printf("Transpose on ND array is not defined. Array has %d dimensions.\n", input_matrix->dims);
		for (i = 0; i < input_matrix->dims; ++i) {
			printf("%d\n", input_matrix->shape[i]);
		}
		exit(EXIT_FAILURE);
	}

	/* If input and output are the same, allocate matrix to temporary array */
	if (input_matrix == *transposed_matrix) {
		new_array_d_2(input_matrix->shape[1], input_matrix->shape[0], &temp);
	}
	/* Otherwise, allocate to output matrix and work on temporary array */
	else {
		new_array_d_2(input_matrix->shape[1], input_matrix->shape[0],
				transposed_matrix);
		temp = *transposed_matrix;
	}

	/* Get dimensions */
	dim1 = dim_size_alloc_d(input_matrix, 0);
	dim2 = dim_size_alloc_d(input_matrix, 1);

	for (i = 0; i < dim1; i++) {
		for (j = 0; j < dim2; j++) {
			temp->data[(j)+(i)*temp->shape[0]] = input_matrix->data[(i) + (j) * input_matrix->shape[0]];
		}
	}

	copy_alloc_d(temp, transposed_matrix);

	return *transposed_matrix;
}

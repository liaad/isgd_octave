/* Header file for add_user */

#ifndef ADD_USER_H
#define ADD_USER_H

#include "lib/matlabstring.h"

/**
 * Add a user to the model (if non-existant)
 */
int add_user_S_1(matlab_string* user_id);

#endif

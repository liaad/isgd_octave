/* Implementation file for predict */

#include "dot_1d.h"
#include "globals.h"
#include "lib/general_functions.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "predict.h"
#include <stdio.h>
#include <stdlib.h>


/**
 * Predict scores for a user and a list of items
 */
double predict_ii_1(int user_row, int item_rows)
{
   tensor_d* ctranspose_arg1 = NULL;
   int end;
   int end_1;
   tensor_d* itemmatrix_1 = NULL;
   tensor_d* itemmatrix_2 = NULL;
   int iter;
   int iter_1;
   tensor_d* mtimes_arg1 = NULL;
   tensor_d* mtimes_arg2 = NULL;
   int range_size;
   int range_size_1;
   double retval;
   tensor_d* usermatrix_1 = NULL;
   tensor_d* usermatrix_2 = NULL;

   //  List of dot products
   // Inlined '$create_and_copy$td'
   if(usermatrix != NULL){
      new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_2);
      copy_td_ptd(usermatrix, &usermatrix_2);
   }
   
   end_1 = usermatrix_2->shape[1];
   // Inlined '$create_and_copy$td'
   if(usermatrix != NULL){
      new_array_helper_d(usermatrix->shape, usermatrix->dims, &usermatrix_1);
      copy_td_ptd(usermatrix, &usermatrix_1);
   }
   
   if(!(user_row <= usermatrix_1->shape[0])){
      printf("Check fail: !(user_row <= usermatrix_1->shape[0])");
      abort();
   }
   
   range_size = end_1 - 1 + 1;
   if(!(end_1 <= usermatrix_1->shape[1])){
      printf("Check fail: !(end_1 <= usermatrix_1->shape[1])");
      abort();
   }
   
   create_td_2(1, range_size, &mtimes_arg1);
   for(iter_1 = 1; iter_1 <= range_size; ++iter_1){
      mtimes_arg1->data[iter_1 - 1] = usermatrix_1->data[(user_row - 1) + (iter_1 + 1 - 1 - 1) * usermatrix_1->shape[0]];
   }
   
   // Inlined '$create_and_copy$td'
   if(itemmatrix != NULL){
      new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_1);
      copy_td_ptd(itemmatrix, &itemmatrix_1);
   }
   
   end = itemmatrix_1->shape[1];
   // Inlined '$create_and_copy$td'
   if(itemmatrix != NULL){
      new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &itemmatrix_2);
      copy_td_ptd(itemmatrix, &itemmatrix_2);
   }
   
   if(!(item_rows <= itemmatrix_2->shape[0])){
      printf("Check fail: !(item_rows <= itemmatrix_2->shape[0])");
      abort();
   }
   
   range_size_1 = end - 1 + 1;
   if(!(end <= itemmatrix_2->shape[1])){
      printf("Check fail: !(end <= itemmatrix_2->shape[1])");
      abort();
   }
   
   create_td_2(1, range_size_1, &ctranspose_arg1);
   for(iter = 1; iter <= range_size_1; ++iter){
      ctranspose_arg1->data[iter - 1] = itemmatrix_2->data[(item_rows - 1) + (iter + 1 - 1 - 1) * itemmatrix_2->shape[0]];
   }
   
   transpose_alloc_d(ctranspose_arg1, &mtimes_arg2);
   retval = dot_1d_tdtd_row_col_1(mtimes_arg1, mtimes_arg2);
   tensor_free_d(&ctranspose_arg1);
   tensor_free_d(&itemmatrix_1);
   tensor_free_d(&itemmatrix_2);
   tensor_free_d(&mtimes_arg1);
   tensor_free_d(&mtimes_arg2);
   tensor_free_d(&usermatrix_1);
   tensor_free_d(&usermatrix_2);
   
   return retval;
}

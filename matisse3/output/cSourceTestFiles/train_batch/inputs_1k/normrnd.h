/* Header file for normrnd */

#ifndef NORMRND_H
#define NORMRND_H

#include <inttypes.h>
#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_d* normrnd_idiui32_1(int mu, double sigma, int m, uint32_t n, tensor_d** restrict out);

#endif

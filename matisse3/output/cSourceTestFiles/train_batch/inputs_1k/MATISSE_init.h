/* Header file for MATISSE_init */

#ifndef MATISSE_INIT_H
#define MATISSE_INIT_H

#include "lib/matisse.h"
#include "lib/matlabstring.h"

/**
 */
void MATISSE_init_2(int* restrict iter, matlab_string* data_values[2000]);

#endif

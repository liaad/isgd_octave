/* Implementation file for normrnd */

#include "custom_rand.h"
#include <inttypes.h>
#include "lib/matisse.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <math.h>
#include "normrnd.h"
#include <stdlib.h>


/**
 */
tensor_d* normrnd_idiui32_1(int mu, double sigma, int m, uint32_t n, tensor_d** restrict out)
{
   int end;
   int i;
   int mtimes_arg1_1;
   float mtimes_arg1_2;
   double pi;

   pi = 3.141592653589793;
   create_td_2(m, (int) n, out);
   end = (*out)->length;
   //  Box-Muller
   mtimes_arg1_1 = -2;
   mtimes_arg1_2 = 2.0f * (float) pi;
   for(i = 0; i < end; ++i){
      float R;
      float theta;
      float u;
      float v;
      float y;
      u = custom_rand_1();
      v = custom_rand_1();
      R = (float) mtimes_arg1_1 * logf(u);
      theta = mtimes_arg1_2 * v;
      y = R * cosf(theta);
      (*out)->data[i] = y * sigma + (double) mu;
   }
   
   
   return *out;
}

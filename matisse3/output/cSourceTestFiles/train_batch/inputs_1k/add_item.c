/* Implementation file for add_item */

#include "add_item.h"
#include "delete_single_index_matrix.h"
#include "find_dynamic1.h"
#include "globals.h"
#include <inttypes.h>
#include "item_to_row.h"
#include "lib/matlabstring.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "newrow.h"
#include <stdlib.h>


/**
 * Add an item to the model (if non-existant)
 */
int add_item_S_1(matlab_string* item_id)
{
   tensor_i* eq_arg1 = NULL;
   tensor_b* find_arg1 = NULL;
   tensor_i* horzcat_arg1 = NULL;
   tensor_S* itemmap_1 = NULL;
   tensor_S* itemmap_2 = NULL;
   tensor_i* itemqueue_1 = NULL;
   tensor_i* itemqueue_index1 = NULL;
   tensor_S* length_arg1 = NULL;
   int retval;
   tensor_d* vertcat_arg1 = NULL;
   tensor_d* vertcat_arg2 = NULL;

   retval = item_to_row_S_1(item_id);
   if(retval < 0){
      int itemmap_index;
      // Inlined '$create_and_copy$tS'
      if(itemmap != NULL){
         new_array_helper_S(itemmap->shape, itemmap->dims, &itemmap_2);
         copy_tS_ptS(itemmap, &itemmap_2);
      }
      
      itemmap_index = itemmap_2->length + 1;
      // Inlined '$create_and_copy$tS'
      if(itemmap != NULL){
         new_array_helper_S(itemmap->shape, itemmap->dims, &itemmap_1);
         copy_tS_ptS(itemmap, &itemmap_1);
      }
      
      MATISSE_reserve_capacity_tSi_tS(itemmap_index, &itemmap_1);
      matlab_string_copy(item_id, &itemmap_1->data[itemmap_index - 1]);
      // Inlined '$create_and_copy$tS'
      if(itemmap_1 != NULL){
         new_array_helper_S(itemmap_1->shape, itemmap_1->dims, &itemmap);
         copy_tS_ptS(itemmap_1, &itemmap);
      }
      
      // Inlined '$create_and_copy$td'
      if(itemmatrix != NULL){
         new_array_helper_d(itemmatrix->shape, itemmatrix->dims, &vertcat_arg1);
         copy_td_ptd(itemmatrix, &vertcat_arg1);
      }
      
      newrow_1(&vertcat_arg2);
      new_col_array_d_2(vertcat_arg1, vertcat_arg2, &itemmatrix);
      // Inlined '$create_and_copy$tS'
      if(itemmap != NULL){
         new_array_helper_S(itemmap->shape, itemmap->dims, &length_arg1);
         copy_tS_ptS(itemmap, &length_arg1);
      }
      
      retval = length_alloc_S(length_arg1);
   }
   
   if(use_imputation){
      int index;
      int iter;
      int iter_1;
      int numel_result;
      int size;
      // Inlined '$create_and_copy$ti'
      if(itemqueue != NULL){
         new_array_helper_i(itemqueue->shape, itemqueue->dims, &itemqueue_1);
         copy_ti_pti(itemqueue, &itemqueue_1);
      }
      
      // Inlined '$create_and_copy$ti'
      if(itemqueue != NULL){
         new_array_helper_i(itemqueue->shape, itemqueue->dims, &eq_arg1);
         copy_ti_pti(itemqueue, &eq_arg1);
      }
      
      new_array_helper_b(eq_arg1->shape, eq_arg1->dims, &find_arg1);
      numel_result = eq_arg1->length;
      for(iter = 0; iter < numel_result; ++iter){
         find_arg1->data[iter] = eq_arg1->data[iter] == retval;
      }
      
      find_dynamic1_tb_row_1(find_arg1, &itemqueue_index1);
      delete_single_index_matrix_titiii_row_2d_1(itemqueue_1, itemqueue_index1, 0, 1, &itemqueue);
      // Inlined '$create_and_copy$ti'
      if(itemqueue != NULL){
         new_array_helper_i(itemqueue->shape, itemqueue->dims, &horzcat_arg1);
         copy_ti_pti(itemqueue, &horzcat_arg1);
      }
      
      size = horzcat_arg1->shape[1];
      create_ti_2(1, 1 + size, &itemqueue);
      index = 1;
      for(iter_1 = 0; iter_1 < size; ++iter_1){
         itemqueue->data[index - 1] = horzcat_arg1->data[iter_1];
         ++index;
      }
      
      itemqueue->data[index - 1] = retval;
   }
   
   tensor_free_i(&eq_arg1);
   tensor_free_b(&find_arg1);
   tensor_free_i(&horzcat_arg1);
   tensor_free_S(&itemmap_1);
   tensor_free_S(&itemmap_2);
   tensor_free_i(&itemqueue_1);
   tensor_free_i(&itemqueue_index1);
   tensor_free_S(&length_arg1);
   tensor_free_d(&vertcat_arg1);
   tensor_free_d(&vertcat_arg2);
   
   return retval;
}

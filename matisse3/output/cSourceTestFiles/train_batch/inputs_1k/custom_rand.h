/* Header file for custom_rand */

#ifndef CUSTOM_RAND_H
#define CUSTOM_RAND_H

#include <inttypes.h>
#include "lib/matisse.h"

/**
 * Return value from 0 to 1
 */
float custom_rand_1();

/**
 */
void uniform_distributed_rand_ui32_2(uint32_t seed, float* restrict y, uint32_t* restrict nextseed);

/**
 */
uint32_t linear_congruential_rand_ui32ui64ui64_1(uint32_t seed, uint64_t a, uint64_t c);

#endif

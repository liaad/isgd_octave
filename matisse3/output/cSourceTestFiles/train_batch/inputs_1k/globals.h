/* Header file for globals */

#ifndef GLOBALS_H
#define GLOBALS_H

#include <inttypes.h>
#include "lib/matlabstring.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

extern uint32_t seed;

extern tensor_d* usermatrix;

extern tensor_i* itemqueue;

extern tensor_S* itemmap;

extern tensor_S* usermap;

extern double learn_rate;

extern double regularization;

extern uint32_t k;

extern tensor_d* itemmatrix;

extern uint8_t update_users;

extern uint8_t use_imputation;

extern double negative_feedback;

extern uint8_t update_items;

extern uint32_t iter;

#endif

/* Header file for newrow */

#ifndef NEWROW_H
#define NEWROW_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_d* newrow_1(tensor_d** restrict retval);

#endif

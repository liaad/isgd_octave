/* Implementation file for dot_1d */

#include "dot_1d.h"
#include "lib/tensor_struct.h"
#include <stdio.h>
#include <stdlib.h>


/**
 */
double dot_1d_tdtd_row_col_1(tensor_d* A, tensor_d* B)
{
   double acc;
   int end;
   int i;

   acc = 0.0;
   end = A->length;
   if(!(B->length >= end)){
      printf("Check fail: !(B->length >= end)");
      abort();
   }
   
   for(i = 0; i < end; ++i){
      acc += A->data[i] * B->data[i];
   }
   
   
   return acc;
}

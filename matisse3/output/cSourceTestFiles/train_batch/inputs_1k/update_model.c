/* Implementation file for update_model */

#include "add_item.h"
#include "add_user.h"
#include "delete_single_index_matrix.h"
#include "fix_general.h"
#include "flip1d.h"
#include "globals.h"
#include <inttypes.h>
#include "lib/general_matrix.h"
#include "lib/matlabstring.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>
#include "update_factors.h"
#include "update_model.h"


/**
 * Update the model with a user-item pair
 */
void update_model_SSib_0(matlab_string* user_id, matlab_string* item_id, int niter, int8_t imputation)
{
   tensor_i* horzcat_arg1 = NULL;
   int i_2;
   int item_row;
   tensor_i* itemqueue_1 = NULL;
   tensor_i* itemqueue_2 = NULL;
   tensor_i* itemqueue_arg1 = NULL;
   tensor_i* itemqueue_index1 = NULL;
   tensor_i* negativeitems = NULL;
   tensor_i* size = NULL;
   tensor_i* source = NULL;
   int user_row;

   user_row = add_user_S_1(user_id);
   item_row = add_item_S_1(item_id);
   //  Perform negative feedback impuation if turned on
   if(imputation){
      int end;
      double fix_result;
      double fix_result_1;
      int index;
      int itemqueue_arg1_dim1;
      int itemqueue_arg1_dim2;
      int iter;
      int iter_1;
      int iter_2;
      int iter_3;
      int iter_4;
      int iter_5;
      int numel_result;
      int size_1;
      int size_2;
      fix_result = fix_general_d_1(negative_feedback);
      create_ti_2(1, (int) fix_result, &itemqueue_arg1);
      for(iter_2 = 1; iter_2 <= fix_result; ++iter_2){
         itemqueue_arg1->data[iter_2 - 1] = iter_2;
      }
      
      // Inlined '$create_and_copy$ti'
      if(itemqueue != NULL){
         new_array_helper_i(itemqueue->shape, itemqueue->dims, &itemqueue_1);
         copy_ti_pti(itemqueue, &itemqueue_1);
      }
      
      size_i(itemqueue_arg1, &size);
      size_multiargs_ti_2_of_2(itemqueue_arg1, &itemqueue_arg1_dim1, &itemqueue_arg1_dim2);
      if(itemqueue_arg1_dim1 == 1 || itemqueue_arg1_dim2 == 1){
         int itemqueue_size1;
         int itemqueue_size2;
         size_multiargs_ti_2_of_2(itemqueue_1, &itemqueue_size1, &itemqueue_size2);
         if(itemqueue_size1 == 1){
            int horzcat_result[2];
            // Inlined 'new_row_i_2'
            horzcat_result[0] = 1;
            horzcat_result[1] = itemqueue_arg1->length;
            
            // Inlined '$create_and_copy$i1x2'
            create_from_matrix_i1x2_i(horzcat_result, &size);
            copy_i1x2_pti(horzcat_result, &size);
         }else if(itemqueue_size2 == 1){
            int horzcat_result_1[2];
            // Inlined 'new_row_i_2'
            horzcat_result_1[0] = itemqueue_arg1->length;
            horzcat_result_1[1] = 1;
            
            // Inlined '$create_and_copy$i1x2'
            create_from_matrix_i1x2_i(horzcat_result_1, &size);
            copy_i1x2_pti(horzcat_result_1, &size);
         }
         
      }
      
      numel_result = itemqueue_arg1->length;
      new_array_ti_i(size, &negativeitems);
      for(iter = 0; iter < numel_result; ++iter){
         negativeitems->data[iter] = itemqueue_1->data[itemqueue_arg1->data[iter] - 1];
      }
      
      flip1d_ti_row_1(negativeitems, &source);
      end = source->shape[1];
      for(iter_5 = 0; iter_5 < end; ++iter_5){
         int i_1;
         int n;
         n = source->data[iter_5];
         for(i_1 = 0; i_1 < niter; ++i_1){
            update_factors_iiibb_0(user_row, n, 0, 1, 0);
         }
         
      }
      
      // Inlined '$create_and_copy$ti'
      if(itemqueue != NULL){
         new_array_helper_i(itemqueue->shape, itemqueue->dims, &itemqueue_2);
         copy_ti_pti(itemqueue, &itemqueue_2);
      }
      
      fix_result_1 = fix_general_d_1(negative_feedback);
      create_ti_2(1, (int) fix_result_1, &itemqueue_index1);
      for(iter_1 = 1; iter_1 <= fix_result_1; ++iter_1){
         itemqueue_index1->data[iter_1 - 1] = iter_1;
      }
      
      delete_single_index_matrix_titiii_row_row_1(itemqueue_2, itemqueue_index1, 0, 1, &itemqueue);
      // Inlined '$create_and_copy$ti'
      if(itemqueue != NULL){
         new_array_helper_i(itemqueue->shape, itemqueue->dims, &horzcat_arg1);
         copy_ti_pti(itemqueue, &horzcat_arg1);
      }
      
      size_1 = horzcat_arg1->shape[1];
      size_2 = negativeitems->shape[1];
      create_ti_2(1, 0 + size_1 + size_2, &itemqueue);
      index = 1;
      for(iter_4 = 0; iter_4 < size_1; ++iter_4){
         itemqueue->data[index - 1] = horzcat_arg1->data[iter_4];
         ++index;
      }
      
      for(iter_3 = 0; iter_3 < size_2; ++iter_3){
         itemqueue->data[index - 1] = negativeitems->data[iter_3];
         ++index;
      }
      
   }
   
   //  iterate iter times with positive (observed) interaction
   for(i_2 = 0; i_2 < niter; ++i_2){
      update_factors_iiiui8ui8_0(user_row, item_row, 1, update_users, update_items);
   }
   
   tensor_free_i(&horzcat_arg1);
   tensor_free_i(&itemqueue_1);
   tensor_free_i(&itemqueue_2);
   tensor_free_i(&itemqueue_arg1);
   tensor_free_i(&itemqueue_index1);
   tensor_free_i(&negativeitems);
   tensor_free_i(&size);
   tensor_free_i(&source);
}

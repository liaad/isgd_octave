/* Implementation file for find_dynamic1 */

#include "find_dynamic1.h"
#include <inttypes.h>
#include "lib/general_matrix.h"
#include "lib/matisse.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>


/**
 */
tensor_i* find_dynamic1_ti_row_1(tensor_i* X, tensor_i** restrict I_out)
{
   tensor_i* I = NULL;
   tensor_i* I_arg1 = NULL;
   int I_arg1_dim1;
   int I_arg1_dim2;
   int counter;
   int end;
   int i;
   int iter_2;
   int iter_3;
   int numel_result;
   tensor_i* size = NULL;

   //  Allocate an array the size of the input array
   if(X->length == X->shape[1]){
      int I_numel_1;
      int iter_1;
      create_ti_2(1, X->length, &I);
      I_numel_1 = I->length;
      for(iter_1 = 0; iter_1 < I_numel_1; ++iter_1){
         I->data[iter_1] = 0;
      }
      
   }else{
      int I_numel;
      int iter;
      create_ti_2(X->length, 1, &I);
      I_numel = I->length;
      for(iter = 0; iter < I_numel; ++iter){
         I->data[iter] = 0;
      }
      
   }
   
   //  Fill array
   counter = 0;
   end = X->length;
   for(i = 1; i <= end; ++i){
      if(X->data[i - 1] != 0){
         ++counter;
         I->data[counter - 1] = i;
      }
      
   }
   
   create_ti_2(1, counter, &I_arg1);
   for(iter_3 = 1; iter_3 <= counter; ++iter_3){
      I_arg1->data[iter_3 - 1] = iter_3;
   }
   
   size_i(I_arg1, &size);
   size_multiargs_ti_2_of_2(I_arg1, &I_arg1_dim1, &I_arg1_dim2);
   if(I_arg1_dim1 == 1 || I_arg1_dim2 == 1){
      int I_size1;
      int I_size2;
      size_multiargs_ti_2_of_2(I, &I_size1, &I_size2);
      if(I_size1 == 1){
         int horzcat_result[2];
         // Inlined 'new_row_i_2'
         horzcat_result[0] = 1;
         horzcat_result[1] = I_arg1->length;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result, &size);
         copy_i1x2_pti(horzcat_result, &size);
      }else if(I_size2 == 1){
         int horzcat_result_1[2];
         // Inlined 'new_row_i_2'
         horzcat_result_1[0] = I_arg1->length;
         horzcat_result_1[1] = 1;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_1, &size);
         copy_i1x2_pti(horzcat_result_1, &size);
      }
      
   }
   
   numel_result = I_arg1->length;
   new_array_ti_i(size, I_out);
   for(iter_2 = 0; iter_2 < numel_result; ++iter_2){
      (*I_out)->data[iter_2] = I->data[I_arg1->data[iter_2] - 1];
   }
   
   tensor_free_i(&I);
   tensor_free_i(&I_arg1);
   tensor_free_i(&size);
   
   return *I_out;
}

/**
 */
tensor_i* find_dynamic1_tb_row_1(tensor_b* X, tensor_i** restrict I_out)
{
   tensor_i* I = NULL;
   tensor_i* I_arg1 = NULL;
   int I_arg1_dim1;
   int I_arg1_dim2;
   int counter;
   int end;
   int i;
   int iter_2;
   int iter_3;
   int numel_result;
   tensor_i* size = NULL;

   //  Allocate an array the size of the input array
   if(X->length == X->shape[1]){
      int I_numel_1;
      int iter_1;
      create_ti_2(1, X->length, &I);
      I_numel_1 = I->length;
      for(iter_1 = 0; iter_1 < I_numel_1; ++iter_1){
         I->data[iter_1] = 0;
      }
      
   }else{
      int I_numel;
      int iter;
      create_ti_2(X->length, 1, &I);
      I_numel = I->length;
      for(iter = 0; iter < I_numel; ++iter){
         I->data[iter] = 0;
      }
      
   }
   
   //  Fill array
   counter = 0;
   end = X->length;
   for(i = 1; i <= end; ++i){
      if(X->data[i - 1] != 0){
         ++counter;
         I->data[counter - 1] = i;
      }
      
   }
   
   create_ti_2(1, counter, &I_arg1);
   for(iter_3 = 1; iter_3 <= counter; ++iter_3){
      I_arg1->data[iter_3 - 1] = iter_3;
   }
   
   size_i(I_arg1, &size);
   size_multiargs_ti_2_of_2(I_arg1, &I_arg1_dim1, &I_arg1_dim2);
   if(I_arg1_dim1 == 1 || I_arg1_dim2 == 1){
      int I_size1;
      int I_size2;
      size_multiargs_ti_2_of_2(I, &I_size1, &I_size2);
      if(I_size1 == 1){
         int horzcat_result[2];
         // Inlined 'new_row_i_2'
         horzcat_result[0] = 1;
         horzcat_result[1] = I_arg1->length;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result, &size);
         copy_i1x2_pti(horzcat_result, &size);
      }else if(I_size2 == 1){
         int horzcat_result_1[2];
         // Inlined 'new_row_i_2'
         horzcat_result_1[0] = I_arg1->length;
         horzcat_result_1[1] = 1;
         
         // Inlined '$create_and_copy$i1x2'
         create_from_matrix_i1x2_i(horzcat_result_1, &size);
         copy_i1x2_pti(horzcat_result_1, &size);
      }
      
   }
   
   numel_result = I_arg1->length;
   new_array_ti_i(size, I_out);
   for(iter_2 = 0; iter_2 < numel_result; ++iter_2){
      (*I_out)->data[iter_2] = I->data[I_arg1->data[iter_2] - 1];
   }
   
   tensor_free_i(&I);
   tensor_free_i(&I_arg1);
   tensor_free_i(&size);
   
   return *I_out;
}

/* Header file for train_batch */

#ifndef TRAIN_BATCH_H
#define TRAIN_BATCH_H

#include "lib/matisse.h"
#include "lib/matlabstring.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 * read csv file and perform batch training
 */
void train_batch_S1000x2i_6(matlab_string* data_values[2000], int iter, tensor_d** restrict usermatrix_1, tensor_d** restrict itemmatrix_1, tensor_d** restrict usermap_as_matrix, tensor_d** restrict itemmap_as_matrix, tensor_i** restrict itemqueue_1, tensor_d** restrict idx);

#endif

/* Implementation file for add_user */

#include "add_user.h"
#include "globals.h"
#include "lib/matlabstring.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "newrow.h"
#include <stdlib.h>
#include "user_to_row.h"


/**
 * Add a user to the model (if non-existant)
 */
int add_user_S_1(matlab_string* user_id)
{
   tensor_S* length_arg1 = NULL;
   int retval;
   tensor_S* usermap_1 = NULL;
   tensor_S* usermap_2 = NULL;
   tensor_d* vertcat_arg1 = NULL;
   tensor_d* vertcat_arg2 = NULL;

   retval = user_to_row_S_1(user_id);
   if(retval < 0){
      int usermap_index;
      // Inlined '$create_and_copy$tS'
      if(usermap != NULL){
         new_array_helper_S(usermap->shape, usermap->dims, &usermap_2);
         copy_tS_ptS(usermap, &usermap_2);
      }
      
      usermap_index = usermap_2->length + 1;
      // Inlined '$create_and_copy$tS'
      if(usermap != NULL){
         new_array_helper_S(usermap->shape, usermap->dims, &usermap_1);
         copy_tS_ptS(usermap, &usermap_1);
      }
      
      MATISSE_reserve_capacity_tSi_tS(usermap_index, &usermap_1);
      matlab_string_copy(user_id, &usermap_1->data[usermap_index - 1]);
      // Inlined '$create_and_copy$tS'
      if(usermap_1 != NULL){
         new_array_helper_S(usermap_1->shape, usermap_1->dims, &usermap);
         copy_tS_ptS(usermap_1, &usermap);
      }
      
      // Inlined '$create_and_copy$td'
      if(usermatrix != NULL){
         new_array_helper_d(usermatrix->shape, usermatrix->dims, &vertcat_arg1);
         copy_td_ptd(usermatrix, &vertcat_arg1);
      }
      
      newrow_1(&vertcat_arg2);
      new_col_array_d_2(vertcat_arg1, vertcat_arg2, &usermatrix);
      // Inlined '$create_and_copy$tS'
      if(usermap != NULL){
         new_array_helper_S(usermap->shape, usermap->dims, &length_arg1);
         copy_tS_ptS(usermap, &length_arg1);
      }
      
      retval = length_alloc_S(length_arg1);
   }
   
   tensor_free_S(&length_arg1);
   tensor_free_S(&usermap_1);
   tensor_free_S(&usermap_2);
   tensor_free_d(&vertcat_arg1);
   tensor_free_d(&vertcat_arg2);
   
   return retval;
}

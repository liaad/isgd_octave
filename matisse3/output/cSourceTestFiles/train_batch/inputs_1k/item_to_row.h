/* Header file for item_to_row */

#ifndef ITEM_TO_ROW_H
#define ITEM_TO_ROW_H

#include "lib/matlabstring.h"

/**
 * Given an item_id, get the correspondent row in the item factor matrix
 */
int item_to_row_S_1(matlab_string* item_id);

#endif

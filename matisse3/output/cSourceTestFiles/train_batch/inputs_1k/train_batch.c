/* Implementation file for train_batch */

#include "globals.h"
#include "lib/matisse.h"
#include "lib/matlabstring.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "randperm.h"
#include <stdlib.h>
#include "train_batch.h"
#include "update_model.h"


/**
 * read csv file and perform batch training
 */
void train_batch_S1000x2i_6(matlab_string* data_values[2000], int iter, tensor_d** restrict usermatrix_1, tensor_d** restrict itemmatrix_1, tensor_d** restrict usermap_as_matrix, tensor_d** restrict itemmap_as_matrix, tensor_i** restrict itemqueue_1, tensor_d** restrict idx)
{
   int end;
   int i;
   int i_1;
   tensor_S* item_ids = NULL;
   int iter_2;
   int iter_3;
   int numel_result;
   int numel_result_1;
   int randperm_arg1;
   tensor_S* str2double_arg1 = NULL;
   tensor_S* str2double_arg1_1 = NULL;
   tensor_S* user_ids = NULL;

   make_empty_tS(&user_ids);
   make_empty_tS(&item_ids);
   end = 1000;
   for(i = 0; i < end; ++i){
      int item_ids_index;
      int user_ids_index;
      user_ids_index = user_ids->length + 1;
      MATISSE_reserve_capacity_tSi_tS(user_ids_index, &user_ids);
      matlab_string_copy(data_values[i], &user_ids->data[user_ids_index - 1]);
      item_ids_index = item_ids->length + 1;
      MATISSE_reserve_capacity_tSi_tS(item_ids_index, &item_ids);
      matlab_string_copy(data_values[i + (2 - 1) * 1000], &item_ids->data[item_ids_index - 1]);
   }
   
   randperm_arg1 = length_alloc_S(user_ids);
   // Assignment from undefined value removed: idx = idx_1
   for(i_1 = 0; i_1 < iter; ++i_1){
      int end_1;
      int iter_4;
      randperm_i_1(randperm_arg1, idx);
      end_1 = (*idx)->shape[1];
      for(iter_4 = 0; iter_4 < end_1; ++iter_4){
         double j;
         j = (*idx)->data[iter_4];
         update_model_SSib_0(user_ids->data[(int) (j - 1.0)], item_ids->data[(int) (j - 1.0)], 1, 0);
      }
      
   }
   
   // Inlined '$create_and_copy$tS'
   if(usermap != NULL){
      new_array_helper_S(usermap->shape, usermap->dims, &str2double_arg1_1);
      copy_tS_ptS(usermap, &str2double_arg1_1);
   }
   
   new_array_helper_d(str2double_arg1_1->shape, str2double_arg1_1->dims, usermap_as_matrix);
   numel_result = str2double_arg1_1->length;
   for(iter_3 = 0; iter_3 < numel_result; ++iter_3){
      (*usermap_as_matrix)->data[iter_3] = atof(str2double_arg1_1->data[iter_3]->data);
   }
   
   // Inlined '$create_and_copy$tS'
   if(itemmap != NULL){
      new_array_helper_S(itemmap->shape, itemmap->dims, &str2double_arg1);
      copy_tS_ptS(itemmap, &str2double_arg1);
   }
   
   new_array_helper_d(str2double_arg1->shape, str2double_arg1->dims, itemmap_as_matrix);
   numel_result_1 = str2double_arg1->length;
   for(iter_2 = 0; iter_2 < numel_result_1; ++iter_2){
      (*itemmap_as_matrix)->data[iter_2] = atof(str2double_arg1->data[iter_2]->data);
   }
   
   // Inlined '$create_and_copy$td'
   if(usermatrix != NULL){
      new_array_helper_d(usermatrix->shape, usermatrix->dims, usermatrix_1);
      copy_td_ptd(usermatrix, usermatrix_1);
   }
   
   // Inlined '$create_and_copy$td'
   if(itemmatrix != NULL){
      new_array_helper_d(itemmatrix->shape, itemmatrix->dims, itemmatrix_1);
      copy_td_ptd(itemmatrix, itemmatrix_1);
   }
   
   // Inlined '$create_and_copy$ti'
   if(itemqueue != NULL){
      new_array_helper_i(itemqueue->shape, itemqueue->dims, itemqueue_1);
      copy_ti_pti(itemqueue, itemqueue_1);
   }
   
   tensor_free_S(&item_ids);
   tensor_free_S(&str2double_arg1);
   tensor_free_S(&str2double_arg1_1);
   tensor_free_S(&user_ids);
}

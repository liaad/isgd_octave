/* Implementation file for fix_general */

#include "fix_general.h"
#include <math.h>


/**
 */
double fix_general_d_1(double X)
{
   double result;

   if(X < 0){
      result = ceil(X);
   }else{
      result = floor(X);
   }
   
   
   return result;
}

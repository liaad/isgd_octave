/* Header file for randperm */

#ifndef RANDPERM_H
#define RANDPERM_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_d* randperm_i_1(int n, tensor_d** restrict p);

#endif

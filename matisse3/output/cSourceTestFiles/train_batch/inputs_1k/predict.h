/* Header file for predict */

#ifndef PREDICT_H
#define PREDICT_H

/**
 * Predict scores for a user and a list of items
 */
double predict_ii_1(int user_row, int item_rows);

#endif

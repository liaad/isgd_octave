/* Header file for sort1 */

#ifndef SORT1_H
#define SORT1_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_i* sort1_ti_col_1(tensor_i* in, tensor_i** restrict out);

#endif

/* Header file for isempty_general */

#ifndef ISEMPTY_GENERAL_H
#define ISEMPTY_GENERAL_H

#include <inttypes.h>
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
int8_t isempty_general_ti_2d_1(tensor_i* X);

#endif

/* Implementation file for main_test */

#include "MATISSE_init.h"
#include "globals.h"
#include "lib/matlabstring.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "lib/utility.h"
#include <stdio.h>
#include <stdlib.h>
#include "train_batch.h"


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Define targets */

#define MULTI_EXEC 0

/* Includes */
#if _WIN32
	#include <windows.h>
	
	double get_time_in_seconds() {
		LARGE_INTEGER t, f;
		QueryPerformanceCounter(&t);
		QueryPerformanceFrequency(&f);
		return (double)t.QuadPart/(double)f.QuadPart;
	}
#endif

#if __linux__
	#include <time.h>
#endif

/**
 */
int main(int argc, char** argv)
{
   matlab_string* data_values[2000];
   tensor_d* idx = NULL;
   int index;
   tensor_d* itemmap_as_matrix = NULL;
   tensor_d* itemmatrix = NULL;
   tensor_i* itemqueue = NULL;
   int iter;
   int numels_v;
   tensor_d* usermap_as_matrix = NULL;
   tensor_d* usermatrix = NULL;

   // Initializations
   // No initialization required for iter
   // No initialization required for seed
   // No initialization required for k
   // No initialization required for learn_rate
   // No initialization required for regularization
   // No initialization required for update_users
   // No initialization required for update_items
   // No initialization required for negative_feedback
   // No initialization required for use_imputation
   create_tS_2(0, 0, &usermap);
   create_tS_2(0, 0, &itemmap);
   create_td_2(0, 0, &usermatrix);
   create_td_2(0, 0, &itemmatrix);
   create_ti_2(0, 0, &itemqueue);
   MATISSE_init_2(&iter, data_values);
   #if MULTI_EXEC
   int iterations_idx;   
   int iterations = 1;
   #endif
   
   // Initialize random seed, in case rand is used
   srand((unsigned) time(NULL));
   
   #if MULTI_EXEC
   if(argc>1)
   {
      iterations = atoi(argv[1]);
   }
   #endif
   #if MULTI_EXEC
   for(iterations_idx = 0; iterations_idx<iterations; iterations_idx++)
   {
   #endif
   train_batch_S1000x2i_6(data_values, iter, &usermatrix, &itemmatrix, &usermap_as_matrix, &itemmap_as_matrix, &itemqueue, &idx);
   #if MULTI_EXEC
   }
   #endif
   write_matrix_2d_td(usermatrix, "usermatrix.txt");
   write_matrix_2d_td(itemmatrix, "itemmatrix.txt");
   write_matrix_2d_td(usermap_as_matrix, "usermap_as_matrix.txt");
   write_matrix_2d_td(itemmap_as_matrix, "itemmap_as_matrix.txt");
   write_matrix_2d_ti(itemqueue, "itemqueue.txt");
   write_matrix_2d_td(idx, "idx.txt");
   index = 0;
   numels_v = usermatrix->length;
   printf("usermatrix:");
   print_dim_alloc_d(usermatrix);
   
   printf(":double=");
   
   while(index<numels_v){
      printf("%.20e ", usermatrix->data[index]);
      index += 1;
   };
   printf("\n");
   index = 0;
   numels_v = itemmatrix->length;
   printf("itemmatrix:");
   print_dim_alloc_d(itemmatrix);
   
   printf(":double=");
   
   while(index<numels_v){
      printf("%.20e ", itemmatrix->data[index]);
      index += 1;
   };
   printf("\n");
   index = 0;
   numels_v = usermap_as_matrix->length;
   printf("usermap_as_matrix:");
   print_dim_alloc_d(usermap_as_matrix);
   
   printf(":double=");
   
   while(index<numels_v){
      printf("%.20e ", usermap_as_matrix->data[index]);
      index += 1;
   };
   printf("\n");
   index = 0;
   numels_v = itemmap_as_matrix->length;
   printf("itemmap_as_matrix:");
   print_dim_alloc_d(itemmap_as_matrix);
   
   printf(":double=");
   
   while(index<numels_v){
      printf("%.20e ", itemmap_as_matrix->data[index]);
      index += 1;
   };
   printf("\n");
   index = 0;
   numels_v = itemqueue->length;
   printf("itemqueue:");
   print_dim_alloc_i(itemqueue);
   
   printf(":int=");
   
   while(index<numels_v){
      printf("%d ", itemqueue->data[index]);
      index += 1;
   };
   printf("\n");
   index = 0;
   numels_v = idx->length;
   printf("idx:");
   print_dim_alloc_d(idx);
   
   printf(":double=");
   
   while(index<numels_v){
      printf("%.20e ", idx->data[index]);
      index += 1;
   };
   printf("\n");
   tensor_free_d(&usermatrix);
   tensor_free_d(&itemmatrix);
   tensor_free_d(&usermap_as_matrix);
   tensor_free_d(&itemmap_as_matrix);
   tensor_free_i(&itemqueue);
   tensor_free_d(&idx);
   
   return 0;
}

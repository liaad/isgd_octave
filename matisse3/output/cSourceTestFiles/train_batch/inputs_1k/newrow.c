/* Implementation file for newrow */

#include "globals.h"
#include <inttypes.h>
#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include "newrow.h"
#include "normrnd.h"
#include <stdlib.h>


/**
 */
tensor_d* newrow_1(tensor_d** restrict retval)
{

   normrnd_idiui32_1(0, 0.01, 1, k, retval);
   
   return *retval;
}

/* Implementation file for custom_rand */

#include "custom_rand.h"
#include "globals.h"
#include <inttypes.h>
#include "lib/matisse.h"
#include <math.h>


/**
 * Return value from 0 to 1
 */
float custom_rand_1()
{
   float y;

   uniform_distributed_rand_ui32_2(seed, &y, &seed);
   
   return y;
}

/**
 */
void uniform_distributed_rand_ui32_2(uint32_t seed, float* restrict y, uint32_t* restrict nextseed)
{
   uint64_t a;
   uint64_t c;

   a = 1664525ul;
   c = 1013904223ul;
   *nextseed = linear_congruential_rand_ui32ui64ui64_1(seed, a, c);
   *y = (float) *nextseed / (float) ((uint32_t) round(4.294967295E9));
}

/**
 */
uint32_t linear_congruential_rand_ui32ui64ui64_1(uint32_t seed, uint64_t a, uint64_t c)
{
   uint64_t seed64;

   //  Use 64-bits to emulate overflow arithmetic
   seed64 = (uint64_t) seed;
   
   return (uint32_t) round(fmod((double) (a * seed64 + c), (double) ((uint64_t) round(4.294967296E9))));
}

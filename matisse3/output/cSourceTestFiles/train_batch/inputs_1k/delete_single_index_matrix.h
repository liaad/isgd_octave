/* Header file for delete_single_index_matrix */

#ifndef DELETE_SINGLE_INDEX_MATRIX_H
#define DELETE_SINGLE_INDEX_MATRIX_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_i* delete_single_index_matrix_titiii_row_row_1(tensor_i* in, tensor_i* indices, int unused_always0, int unused_always1, tensor_i** restrict out);

/**
 */
tensor_i* delete_single_index_matrix_titiii_row_2d_1(tensor_i* in, tensor_i* indices, int unused_always0, int unused_always1, tensor_i** restrict out);

#endif

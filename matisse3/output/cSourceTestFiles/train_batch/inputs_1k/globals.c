/* Implementation file for globals */

#include "globals.h"
#include "lib/matlabstring.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>


uint32_t seed;
tensor_d* usermatrix;
tensor_i* itemqueue;
tensor_S* itemmap;
tensor_S* usermap;
double learn_rate;
double regularization;
uint32_t k;
tensor_d* itemmatrix;
uint8_t update_users;
uint8_t use_imputation;
double negative_feedback;
uint8_t update_items;
uint32_t iter;
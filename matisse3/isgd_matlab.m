% ISGD - Incremental Stochastic Gradient Descent
%
% Parameters:
% k - no. of latent features
% iter - no. of iterations
% learn_rate - the learn rate (or step size)
% regularization - regularization factor
% update_users - (boolean) update user factors
% update_items - (boolean) update item factors
% negative_feedback - (int) number of items to impute as negative feedback, for
%                     each positive observation (only works if update_users=true)
% datafile - csv file with user-item pairs for training
%
% Usage: Update parameters above, then load this script in octave
% To train the model, run train()
% To perform recommendation to user u, run recommend(u, n), where n is the
%  number of recommended items
% Output of recommend(u,n) is an ordered list of items
% To get the scores, run [items, scores] = recommend(u, n)
%
% Incremental updates are possible with update_factors(user_id, item_id)
1;
global k
global iter
global learn_rate
global regularization
global update_users
global update_items
global negative_feedback
global datafile
% Data structures
global usermap
% maps user ids (strings) to rows in user factor matrix
global itemmap
% maps item ids (strings) to rows in item factor matrix
global usermatrix
% the user (u x k) factor matrix
global itemmatrix
% the item (i x k) factor matrix
global itemqueue
% the item queue for negative feedback imputation
global use_imputation
% Default settings
k = 50
iter = 3
learn_rate = 0.1
regularization = 0.1
update_users = true
update_items = true
negative_feedback = 1
use_imputation = update_users && negative_feedback > 0
datafile = './data/palco_playlists.csv.1k'
% Structure init
usermap = {}
itemmap = {}
usermatrix = []
itemmatrix = []
itemqueue = []
% read csv file line by line, and perform incremental model updates
function isgd()
   global datafile iter
   [handler, err_msg] = fopen(datafile)
   if (handler < 0)
      disp(err_msg)
      exit
   end
   line = fgetl(handler)
   while ischar(line)
      words = strsplit(line)
      user_id = words{1}
      item_id = words{2}
      update_model(user_id, item_id, iter, use_imputation)
      line = fgetl(handler)
   end
   fclose(datafile)
end

% read csv file and perform batch training
function train_batch()
   global datafile iter
   user_ids = {}
   item_ids = {}
   [handler, err_msg] = fopen(datafile)
   if (handler < 0)
      disp(err_msg)
      exit
   end
   line = fgetl(handler)
   while ischar(line)
      words = strsplit(line)
      user_ids{end + 1} = words{1}
      item_ids{end + 1} = words{2}
      line = fgetl(handler)
   end
   for i = 1:iter
      idx = randperm(length(user_ids))
      for j = idx
         update_model(user_ids{j}, item_ids{j}, 1, false)
      end
   end
   fclose(datafile)
end

% Update the model with a user-item pair
function update_model(user_id, item_id, niter, imputation)
   global update_users update_items negative_feedback itemqueue
   user_row = add_user(user_id)
   item_row = add_item(item_id)
   % Perform negative feedback impuation if turned on
   if (imputation)
      negativeitems = itemqueue(1:negative_feedback)
      for n = flip(negativeitems)
         for i = 1:niter
            update_factors(user_row, n, 0, true, false)
         end
      end
      itemqueue(1:negative_feedback) = []
      itemqueue = [itemqueue, negativeitems]
   end
   % iterate iter times with positive (observed) interaction
   for i = 1:niter
      update_factors(user_row, item_row, 1, update_users, update_items)
   end
end

% Add a user to the model (if non-existant)
function [retval] = add_user(user_id)
   global usermap usermatrix
   retval = user_to_row(user_id)
   if (length(retval) == 0)
      usermap{end + 1} = user_id
      usermatrix = [usermatrix; newrow()]
      retval = length(usermap)
   end
end

% Add an item to the model (if non-existant)
function [retval] = add_item(item_id)
   global itemmap itemmatrix use_imputation itemqueue
   retval = item_to_row(item_id)
   if (length(retval) == 0)
      itemmap{end + 1} = item_id
      itemmatrix = [itemmatrix; newrow()]
      retval = length(itemmap)
   end
   if (use_imputation)
      itemqueue(find(itemqueue == retval)) = []
      itemqueue = [itemqueue, retval]
   end
end

% Given a user_id, get the correspondent row in the user factor matrix
function [retval] = user_to_row(user_id)
   global usermap
   retval = find(ismember(usermap, user_id))
end

% Given an item_id, get the correspondent row in the item factor matrix
function [retval] = item_to_row(item_id)
   global itemmap
   retval = find(ismember(itemmap, item_id))
end

% Initialize a new row in a factor matrix with random values close to 0 (using gaussian)
function [retval] = newrow()
   global k
   retval = normrnd(0, 0.01, 1, k)
end

% Update user and item factors
function update_factors(user_row, item_row, truescore, update_users, update_items)
   global usermatrix itemmatrix k learn_rate regularization
   err = truescore - predict(user_row, item_row)
   for i = 1:k
      if update_users
         delta_u = err * itemmatrix(item_row, i) - regularization * usermatrix(user_row, i)
         usermatrix(user_row, i) = usermatrix(user_row, i) + learn_rate * delta_u
      end
      if update_items
         delta_i = err * usermatrix(user_row, i) - regularization * itemmatrix(item_row, i)
         itemmatrix(item_row, i) = itemmatrix(item_row, i) + learn_rate * delta_i
      end
   end
end

% Predict scores for a user and a list of items
function [retval] = predict(user_row, item_rows)
   global usermatrix itemmatrix
   % List of dot products
   retval = usermatrix(user_row, :) * itemmatrix(item_rows, :)'
end

% Obtain a recommendation list with size n for a given user id
function [recs, scores] = recommend(user_id, n)
   global itemmap
   user_row = user_to_row(user_id)
   retval = {}
   if (length(user_row) ~= 0)
      % Recommendations are sorted by proximity to value 1
      [val, ord] = sort(abs(1 - predict(user_row, 1:length(itemmap))))
      recs = row_to_item(ord(1:n))'
      scores = num2cell(val(1:n))'
   end
end

% Get user ids from list of rows in user factor matrix
function [retval] = row_to_user(rows)
   global usermap
   retval = usermap(rows)
end

% Get item ids from list of rows in item factor matrix
function [retval] = row_to_item(rows)
   global itemmap
   retval = itemmap(rows)
end


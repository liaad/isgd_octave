% Update user and item factors
function update_factors(user_row, item_row, truescore, update_users, update_items)
   global usermatrix itemmatrix k learn_rate regularization;
   err = truescore - predict(user_row, item_row);
   for i = 1:k
      if update_users
         delta_u = err * itemmatrix(item_row, i) - regularization * usermatrix(user_row, i);
         usermatrix(user_row, i) = usermatrix(user_row, i) + learn_rate * delta_u;
      end
      if update_items
         delta_i = err * usermatrix(user_row, i) - regularization * itemmatrix(item_row, i);
         itemmatrix(item_row, i) = itemmatrix(item_row, i) + learn_rate * delta_i;
      end
   end
end

% Update the model with a user-item pair
function update_model(user_id, item_id, niter, imputation)
   global update_users
   global update_items
   global negative_feedback
   global itemqueue;
   user_row = add_user(user_id);
   item_row = add_item(item_id);
   % Perform negative feedback impuation if turned on
   if (imputation)
      negativeitems = itemqueue(1:negative_feedback);
      for n = flip(negativeitems)
         for i = 1:niter
            update_factors(user_row, n, 0, true, false);
         end
      end
      itemqueue(1:negative_feedback) = [];
      itemqueue = [itemqueue, negativeitems];
   end
   % iterate iter times with positive (observed) interaction
   for i = 1:niter
      update_factors(user_row, item_row, 1, update_users, update_items);
   end
end

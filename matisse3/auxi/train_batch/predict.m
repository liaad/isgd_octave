% Predict scores for a user and a list of items
function [retval] = predict(user_row, item_rows)
   global usermatrix itemmatrix;
   % List of dot products
   retval = usermatrix(user_row, :) * itemmatrix(item_rows, :)';
end

% Given a user_id, get the correspondent row in the user factor matrix
function [retval] = user_to_row(user_id)
   global usermap
   retval = find(user_id == usermap);
   if isempty(retval),
      retval = -1;
   else
      retval = retval(1);
   end
end
